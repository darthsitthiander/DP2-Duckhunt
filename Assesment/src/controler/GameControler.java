package controler;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Observable;
import javax.swing.SwingUtilities;

import model.BackgroundAnimation;
import model.BattleField;
import model.BouncingProjectile;
import model.EnemyShip;
import model.FriendlyShip;
import model.Projectile;
import model.WelcomeScreenAnimation;

import view.GameControlPanel;
import view.GamePanel;
import view.GameViewPanel;
import view.MainFrame;

public class GameControler extends Observable {
	private MainFrame mainFrame;
	private BattleField battleField;
	private BackgroundAnimation backgroundAnimation;
	private WelcomeScreenAnimation welcomeScreenAnimation;
	private boolean soundEnabled;

	public GameControler() {
		/* the background animation, welcomescreenanimation and mainframe gets intialized
		 and th mainframe is set visible.*/
		backgroundAnimation = new BackgroundAnimation(this);
		welcomeScreenAnimation = new WelcomeScreenAnimation(this);
		soundEnabled = true;
		mainFrame = new MainFrame(this);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				mainFrame.buildAndShowGui();
			}
		});
	}

	/*Called from the welcomepanel once it is set visible so it starts loading the data and intializes
	 * the battlefield if it is null.*/
	public void loadLevelData() {
		if(battleField==null){
			battleField = new BattleField(this);
		}
		battleField.loadLevelData();
	}

	// intialize the gamepanel and return it so the mainFrame can swith it to
	// their contentPane.
	public GamePanel getAndIntializeGamePanel() {
		GameViewPanel gameViewPanel = new GameViewPanel(mainFrame, this);
		addObserver(gameViewPanel);
		GameControlPanel gameControlerPanel = new GameControlPanel(this, battleField.getGameSpeed());
		addObserver(gameControlerPanel);
		return new GamePanel(gameViewPanel, gameControlerPanel);
	}

	// getters for the ships and projectiles on the field.
	public ArrayList<EnemyShip> getEnemyShipsOnBattleField() {
		return battleField.getEnemyShipsOnBattleField();
	}

	public ArrayList<FriendlyShip> getFriendlyShipsOnBattleField() {
		return battleField.getFriendlyShipsOnBattleField();
	}

	public ArrayList<Projectile> getProjectilesOnBattleField() {
		return battleField.getProjectilesOnBattleField();
	}

	// getter and setter for the currently selected friendlyShip.
	public void setSelectedShip(FriendlyShip newSelectedShip) {
		battleField.setSelectedShip(newSelectedShip);
		setChanged();
		notifyObservers();
	}

	public FriendlyShip getSelectedShip() {
		return battleField.getSelectedShip();
	}
	/*getters to get the projectiles and moon on the background so tthey can be painted
	 * on the paintCOmponent method in the gameView, welcomePanel, levelSwitch panel and gamecompleted panel.*/
	public ArrayList<Projectile> getBackgroundStarsFromBattleField() {
		return backgroundAnimation.getBackgroundStars();
	}

	public Projectile getBackgroundMoonFromBattleField() {
		return backgroundAnimation.getBackgroundMoon();
	}

	// methods to start, pause and reset the game.
	public void startGame() {
		battleField.startGame();
	}

	public void pauseGame() {
		battleField.pauseGame();
	}
	
	//notify the observers so the controlpanel can reset its buttons.
	public void resetLevel() {
		battleField.resetLevel();
		setChanged();
		notifyObservers("resetLevel");
	}

	/* method that gets called from the battleField and WelcomeScreenAnimation
	 to update the view*/
	public void updateFromModelToView() {
		setChanged();
		notifyObservers();
	}
	
	//when there is something to say form a model to view, then this method is called.
	public void updateFromModelToView(String string) {
		setChanged();
		notifyObservers(string);
	}

	// getter and setter for the gamespeed
	public void setGameSpeed(int speed) {
		battleField.setGameSpeed(speed);
		backgroundAnimation.setGameSpeed(speed);
	}

	public int getGameSpeed() {
		return battleField.getGameSpeed();
	}

	/* method that gets called when the player clicks with the mouse within the
	 game screen. if sets the selected ship and moves them.*/
	public void gameViewMouseClicked(MouseEvent mouseEvent) {
		if (battleField == null || !battleField.gameIsPlaying()) {
			return;
		}
		boolean selectedChanged = false;
		boolean needToMove = false;
		Point pointToGo = new Point(0, 0);
		FriendlyShip selectShip = null;
		FriendlyShip moveShip = null;
		int numClickedOnShips = 0;
		Rectangle mousePos = new Rectangle(mouseEvent.getX(),
				mouseEvent.getY(), 1, 1);
		for (FriendlyShip friendlyShip : getFriendlyShipsOnBattleField()) {
			if (friendlyShip.intersects(mousePos)) {
				if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
					selectShip = friendlyShip;
					selectedChanged = true;
				} else if (SwingUtilities.isRightMouseButton(mouseEvent)) {
					selectedChanged = true;
				}
				numClickedOnShips++;
			} else if (SwingUtilities.isLeftMouseButton(mouseEvent)
					&& getSelectedShip() != null
					&& getSelectedShip().equals(friendlyShip)) {
				pointToGo = new Point(mouseEvent.getX(), mouseEvent.getY());
				moveShip = friendlyShip;
				needToMove = true;
			}
		}
		if (selectedChanged) {
			setSelectedShip(selectShip);
		} else if (needToMove && numClickedOnShips == 0) {
			moveShip.setTarget(pointToGo);
		} else {
			if (SwingUtilities.isRightMouseButton(mouseEvent)
					&& battleField.getSelectedShip() != null) {
				battleField.shootBouncingProjectile(
						battleField.getSelectedShip(), mousePos);
			}
		}
	}

	/*Tell the gameview to play a shoting or explosion sound, only if the sound is enabled.*/
	public void playShootSound() {
		if (soundEnabled) {
			setChanged();
			notifyObservers("playShootSound");
		}
	}

	public void playExplosionSound() {
		if (soundEnabled) {
			setChanged();
			notifyObservers("playExplosionSound");
		}
	}
	
	//getters to be used in teh paintCOmponent on the welcomepanel to get the right data to paint.
	public FriendlyShip getWelcomeAnimationFriendlyShip() {
		return welcomeScreenAnimation.getFriendlyShip();
	}

	public ArrayList<BouncingProjectile> getWelcomeAnimationBouncingProjectiles() {
		return welcomeScreenAnimation.getBouncingProjectiles();
	}
	
	/*Notify the GameView and GameControler to show that you are defeated, and reset the buttons.*/
	public void showDefeated(){
		setChanged();
		notifyObservers("defeated");
	}
	
	/*Notify the GameView if he needs to show the nextLevel panel or the gamecompleted panel. called from
	 * battlefield when there are no enemyships left.*/
	public void showNextLevelScreen(int nextLevel) {
		battleField.clearBattleField();
		setChanged();
		if (battleField.getMaxLevel() >= nextLevel) {
			notifyObservers("showNextLevel");
		} else {
			notifyObservers("gameCompleted");
		}
	}

	/*Tell the battlefield to load the next level, and after that inform the levelswitchpanel that the level is loaded
	 * so the player can press enter to start it, and tell the controler to reset its buttons.*/
	public void loadNextLevel() {
		battleField.loadNextLevel();
		setChanged();
		notifyObservers("LevelLoaded");
		mainFrame.getGamePanel().getGameControlPanel().prepaireNextLevel();
	}
	

	public void setSoundEnabled(boolean enabled) {
		soundEnabled = enabled;
	}
	
	public int getCurrentLevel(){
		return battleField.getCurrentLevel();
	}
	
	//called by the mainframe to stop the animation when the game is started and start it when you return back to it.
	public void stopWelcomeScreenAnimation(){
		welcomeScreenAnimation = null;
	}
	
	public void startWelcomeScreenAnimation(){
		welcomeScreenAnimation = new WelcomeScreenAnimation(this);
	}
}
