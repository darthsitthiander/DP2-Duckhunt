package view;

import java.awt.Color;  
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.BouncingProjectile;
import model.FriendlyShip;
import model.Projectile;

import controler.GameControler;

@SuppressWarnings("serial")
public class WelcomePanel extends JPanel implements Observer {
	//Panel used to show the title and sends you to the gamepanel after you pressed enter.
	private MainFrame mainFrame;
	private GameControler controler;
	private boolean loadingCompleted, reloading;
	private JLabel loadingLabel;
	private Image friendlyShipImage;
	public WelcomePanel(MainFrame newMainFrame, GameControler newControler){
		mainFrame = newMainFrame;
		controler = newControler;
		loadingCompleted = false;
		reloading = false;
		setPreferredSize(new Dimension(500,630));
		setLayout(new FlowLayout());
		setBackground(Color.black);
		
		JLabel titleLabel = new JLabel("WELCOME TO SPACE WARS!", JLabel.CENTER);
		titleLabel.setPreferredSize(new Dimension(480,60));
		titleLabel.setFont(new Font("Ariel", Font.BOLD, 33));
		titleLabel.setForeground(Color.red);
		
		JLabel instructionsLabel = new JLabel("<html>Fight with your battleships against the battleships of the enemy! "
				 + "Move your ship by selecting it and move it to a location by click with the left mouse button. If you get close enaugh to an enemy, "
				 + "then it will automaticly fire a projectile towards him. If you press with your right mouse button, "
				 + "then a bouncing projectile will be fired. It is twice as big, bounces 3 times against the walls "
				 + "and will stay longer in the battlefield then a regular projectile. If a ship is above the moon and fires a projectile, "
				 + "then 8 projectiles will be shot as a bonus. You can change the speed of the game by moving the slider, "
				 + "and enable or disable the sounds with the checkbox. You win the level if you defeat the ships of the enemy "
				 + "before they defeat yours. If you get defeated, then you can reset the level. If you make it through all the levels, "
				 + "then you have completed the game.</html>", JLabel.CENTER);
		instructionsLabel.setPreferredSize(new Dimension(480,480));
		instructionsLabel.setFont(new Font("Ariel", Font.BOLD, 20));
		instructionsLabel.setForeground(Color.gray);
		
		loadingLabel = new JLabel("Loading the levels...", JLabel.CENTER);
		loadingLabel.setPreferredSize(new Dimension(480,50));
		loadingLabel.setFont(new Font("Ariel", Font.BOLD, 30));
		loadingLabel.setForeground(Color.green);
		
		add(titleLabel);
		add(instructionsLabel);
		add(loadingLabel);
		
		//load friendlyShipImage
		friendlyShipImage = new ImageIcon("resources/chaser.png").getImage();
		
		//start the game when the player presses enter and the loading is completed.
		addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent evt){
				if(evt.getKeyCode() == KeyEvent.VK_ENTER && loadingCompleted){
					mainFrame.startGame();
				}
			}
		});
		setFocusable(true);
	}
	/*Called from mainFrame after the welcomePanel is set to visible.*/
	public void startLoading(){
		controler.loadLevelData();
	}
	
	/*To reload te levels, some changes are needed. the reloading is used so the gamePanel 
	 * doesn;t get re-intialized again, and a different text is nicer.*/
	public void prepaireReloadingLevels(){
		loadingCompleted = false;
		reloading = true;
		loadingLabel.setText("Reloading the levels...");
	}
	/*Paint the background and the ship and bouncing projectiles.*/
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		try{
			Projectile moon = controler.getBackgroundMoonFromBattleField();
			if(moon != null){
				g.setColor(moon.getProjectileColor());
				g.fillOval(moon.x, moon.y, moon.width, moon.height);
			}
			for(Projectile star:controler.getBackgroundStarsFromBattleField()){
				g.setColor(star.getProjectileColor());
				g.fillRect(star.x, star.y, star.width, star.height);
			}
			for(BouncingProjectile shot:controler.getWelcomeAnimationBouncingProjectiles()){
				g.setColor(shot.getProjectileColor());
				g.fillOval(shot.x, shot.y, shot.width, shot.height);
			}
		}catch(Exception e){}
		FriendlyShip friendlyShip = controler.getWelcomeAnimationFriendlyShip();
		g.drawImage(friendlyShipImage, friendlyShip.getLocation().x, friendlyShip.getLocation().y, null);
	}
	
	/*Gets called in the welcomeanimationloop and backgroundloop to repaint after a changehappened.
	 * Also used by the controler f the battlefield has loaded the levels so the player can press enter and begin.*/
	@Override
	public void update(Observable arg0, Object arg1) {
		String command = (String) arg1;
		if(command!=null && command.equals("levelsLoaded")){
			//this panel will be reloading if the user visits the panel for the 2nd time.
			if(!reloading){
				mainFrame.setGamePanel(controler.getAndIntializeGamePanel());	
			}
			loadingCompleted = true;
			reloading = false;
			loadingLabel.setText("Press ENTER to start the game.");
		}
		repaint();
	}
}
