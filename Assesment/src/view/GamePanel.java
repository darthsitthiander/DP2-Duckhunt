package view;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GamePanel extends JPanel {
	private GameControlPanel gameControlPanel;
	/*Set to contentpane during the game to display the gameview and gamecontrol panels.*/
	public GamePanel(GameViewPanel gameViewPanel, GameControlPanel newGameControlPanel){
		gameControlPanel = newGameControlPanel;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(gameViewPanel);
		add(gameControlPanel);
	}
	
	/*Used to prepaire the controlpanel for the next level by setting the play buttontext to 'Play!'*/
	public GameControlPanel getGameControlPanel(){
		return gameControlPanel;
	}
}
