package view;

import java.awt.Color; 
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Projectile;
import controler.GameControler;

@SuppressWarnings("serial")
public class LevelSwitchPanel extends JPanel implements Observer {
	
	private MainFrame mainFrame;
	private int nextLevel;
	private boolean loadingCompleted;
	private JLabel infoLabel;
	private GameControler controler;
	/*Intialize and set preferredsize. Add labels and key listener to start the next level.
	 * when it is loaded.*/
	public LevelSwitchPanel(MainFrame newMainFrame, GameControler newControler){
		mainFrame = newMainFrame;
		controler = newControler;
		nextLevel = controler.getCurrentLevel() + 1;
		setPreferredSize(new Dimension(500,630));
		setBackground(Color.gray);
		JLabel titleLabel = new JLabel("<html>You cleared level " + (nextLevel - 1) + ".</html>");
		titleLabel.setPreferredSize(new Dimension(500, 200));
		titleLabel.setFont(new Font("Ariel", Font.BOLD, 30));
		titleLabel.setForeground(Color.red);
		
		infoLabel = new JLabel("<html>Please wait for level " + nextLevel + " to load.</html>");
		infoLabel.setPreferredSize(new Dimension(500, 200));
		infoLabel.setFont(new Font("Ariel", Font.BOLD, 20));
		infoLabel.setForeground(Color.blue);
		
		add(titleLabel);
		add(infoLabel);
		
		addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent evt){
				if(evt.getKeyCode() == KeyEvent.VK_ENTER && loadingCompleted){
					mainFrame.startGame();
				}
			}
		});
		setFocusable(true);
		/*Tell the controler to start loading the next level.*/
		Thread loadLevelThread = new Thread(new Runnable(){
			@Override
			public void run() {
				controler.loadNextLevel();
			}
		});
		loadLevelThread.start();
	}
	
	/*Paint the backgroundanimation.*/
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		try{
			Projectile moon = controler.getBackgroundMoonFromBattleField();
			if(moon != null){
				g.setColor(moon.getProjectileColor());
				g.fillOval(moon.x, moon.y, moon.width, moon.height);
			}
			for(Projectile star:controler.getBackgroundStarsFromBattleField()){
				g.setColor(star.getProjectileColor());
				g.fillRect(star.x, star.y, star.width, star.height);
			}
		}catch(Exception e){}
	}
	
	/*repaint animation and set loading complete so the user cna press enter to go to the next level.
	 * Use <html> to get the text of the label to break when it gets outside its preferredsize.*/
	@Override
	public void update(Observable arg0, Object arg1) {
		String command = (String) arg1;
		if(command!=null && command.equals("LevelLoaded")){
			loadingCompleted = true;
			infoLabel.setText("<html>Press ENTER to start level " + nextLevel + ".</html>");
		}
		repaint();
	}

}
