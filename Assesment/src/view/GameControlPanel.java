package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.Ship;

import controler.GameControler;

@SuppressWarnings("serial")
public class GameControlPanel extends JPanel implements Observer {
	private JButton playPauseButton, resetButton;
	private JCheckBox soundCheckbox;
	private JSlider speedSlider;
	private GameControler controler;
	private long timeToLoadProjectile, timeToLoadBouncingProjectile, loadingTimeProjectile, loadingTimeBouncingProjectile;
	/*Panel with the buttons, slider and enable sound option. Also dsplys the loading time of the projectiles.*/
	public GameControlPanel(GameControler newControler, int gameSpeed){
		controler = newControler;
		setPreferredSize(new Dimension(500,130));
		
		JLabel projectileLoading = new JLabel("Projectile loading:");
		projectileLoading.setPreferredSize(new Dimension(160,30));
		
		JLabel bouncingProjectileLoading = new JLabel("Bouncing projectile loading:");
		bouncingProjectileLoading.setPreferredSize(new Dimension(160,30));
		
		playPauseButton = new JButton("Play!");
		playPauseButton.setPreferredSize(new Dimension(240,30));
		playPauseButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(playPauseButton.getText().equals("Play!")){
					playPauseButton.setText("Pause");
					controler.startGame();
				} else {
					playPauseButton.setText("Play!");
					controler.pauseGame();
				}
			}
		});
		
		resetButton = new JButton("Reset Level");
		resetButton.setPreferredSize(new Dimension(240,30));
		resetButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				controler.resetLevel();
				playPauseButton.setText("Play!");
				playPauseButton.setEnabled(true);
			}
		});
		
		//turn around game speed becuse the jSlider needs to be turned around.
		gameSpeed = ((gameSpeed-30) * -1) + 30;
		speedSlider = new JSlider(10, 50, gameSpeed);
		speedSlider.setPreferredSize(new Dimension(420, 40));
		speedSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent source) {
				//turn around the JSlider, left is slow and right is fast.
				controler.setGameSpeed(((speedSlider.getValue()-30) * -1) + 30);
			}
		});
		
		soundCheckbox = new JCheckBox("Sound");
		soundCheckbox.setPreferredSize(new Dimension(70, 40));
		soundCheckbox.setSelected(true);
		soundCheckbox.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				controler.setSoundEnabled(soundCheckbox.isSelected());
			}
		});
		add(projectileLoading);
		add(Box.createRigidArea(new Dimension(70,50)));
		add(bouncingProjectileLoading);
		add(Box.createRigidArea(new Dimension(70,50)));
		add(playPauseButton);
		add(resetButton);
		add(speedSlider);
		add(soundCheckbox);
	}
	
	/*Called when a new level has loaded so the button displays corerctly.*/
	public void prepaireNextLevel(){
		playPauseButton.setText("Play!");
	}
	
	/*Paint the loadingbars.*/
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.black);
		g.drawRect(5, 5, 490, 50);
		g.drawRect(169, 14, 71, 31);
		g.drawRect(419, 14, 71, 31);
		
		g.setColor(Color.red);
		g.fillRect(170, 15, 70, 30);
		g.fillRect(420, 15, 70, 30);
		
		g.setColor(Color.green);
		g.fillRect(170, 15, (int)(70*((double)timeToLoadProjectile/loadingTimeProjectile)), 30);
		g.fillRect(420, 15, (int)(70*((double)timeToLoadBouncingProjectile/loadingTimeBouncingProjectile)), 30);
	}
	
	/*called in the gameloop. updates the loadingbars and if defeated then the pausebutton is disabled.
	 * On every update, repaint.*/
	@Override
	public void update(Observable arg0, Object arg1) {
		Ship localSelectedShip = controler.getSelectedShip();
		if(localSelectedShip!=null){
			loadingTimeProjectile = localSelectedShip.getLoadingTimeProjectile();
			loadingTimeBouncingProjectile = localSelectedShip.getLoadingTimeBouncingProjectile();
			timeToLoadProjectile = System.currentTimeMillis() - controler.getSelectedShip().getLastTimeShotProjectile();
			if(timeToLoadProjectile > loadingTimeProjectile) timeToLoadProjectile = loadingTimeProjectile;
			timeToLoadBouncingProjectile = System.currentTimeMillis() - controler.getSelectedShip().getLastTimeShotBouncingProjectile();
			if(timeToLoadBouncingProjectile > loadingTimeBouncingProjectile) timeToLoadBouncingProjectile = loadingTimeBouncingProjectile;
		} else {
			loadingTimeProjectile = 0;
			loadingTimeBouncingProjectile = 0;
			timeToLoadProjectile = 0;
			timeToLoadBouncingProjectile = 0;
		}
		String command = (String) arg1;
		if(command!=null && command.equals("defeated")){
			playPauseButton.setEnabled(false);
		}
		repaint();
	}
}
