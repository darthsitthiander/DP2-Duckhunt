package view;

import java.awt.Color; 
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.EnemyShip;
import model.FriendlyShip;
import model.Projectile;
import controler.GameControler;

@SuppressWarnings("serial")
public class GameViewPanel extends JPanel implements Observer {
	private MainFrame mainFrame;
	private GameControler controler;
	private Image friendlyShipImage, selectedFriendlyShipImage, enemyShipImage;
	private File shootSound, explosionSound;
	private Clip clip;
	private JLabel defeatedLabel;
	/*Shows the game and if the player pressed on a mousebutton within the jpanel, it gets send through the Controler.
	 * Also shows a JLabel if the player is defeated.*/
	public GameViewPanel(MainFrame newMainFrame, GameControler newControler){
		mainFrame = newMainFrame;
		controler = newControler;
		
		defeatedLabel = new JLabel("<html>You have lost this match. Press Reset level to try again.</html>");
		defeatedLabel.setPreferredSize(new Dimension(500,500));
		defeatedLabel.setFont(new Font("Ariel", Font.BOLD, 30));
		defeatedLabel.setForeground(Color.blue);
		defeatedLabel.setVisible(false);
		add(defeatedLabel);
		
		//import the images and sounds.
		friendlyShipImage = new ImageIcon("resources/chaser.png").getImage();
		selectedFriendlyShipImage = new ImageIcon("resources/chaser_selected.png").getImage();
		enemyShipImage = new ImageIcon("resources/target.png").getImage();
		
		shootSound = new File("resources/shoot.wav");
		explosionSound = new File("resources/explode.wav");
		
		addMouseListener(new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				controler.gameViewMouseClicked(mouseEvent);
			}
		});
		setPreferredSize(new Dimension(500,500));
		setBackground(Color.black);
	}
	
	/*refreshes the screen and paints the ships and projectiles on the screen.
	 The projectiles are painted first so it looks like the projectiles comes from under the ship instead of above them.*/
	public void paintComponent(Graphics g){
		super.paintComponent(g);
			Projectile moon = controler.getBackgroundMoonFromBattleField();
			if(moon != null){
				g.setColor(moon.getProjectileColor());
				g.fillOval(moon.x, moon.y, moon.width, moon.height);
			}
			try{
				Iterator<Projectile> backgroundStarsIterator = controler.getBackgroundStarsFromBattleField().iterator();
				while(backgroundStarsIterator.hasNext()){
					Projectile star = backgroundStarsIterator.next();
					g.setColor(star.getProjectileColor());
					g.fillRect(star.x, star.y, star.width, star.height);
				}
				Iterator<Projectile> projectilesIterator = controler.getProjectilesOnBattleField().iterator();
				while(projectilesIterator.hasNext()){
					Projectile shot = projectilesIterator.next();
					g.setColor(shot.getProjectileColor());
					g.fillOval(shot.x, shot.y, shot.width, shot.height);
				}
				Iterator<FriendlyShip> friendlyShipsIterator = controler.getFriendlyShipsOnBattleField().iterator();
				while(friendlyShipsIterator.hasNext()){
					FriendlyShip friendlyShip = friendlyShipsIterator.next();
					if(friendlyShip.equals(controler.getSelectedShip())){
						g.drawImage(selectedFriendlyShipImage, friendlyShip.getLocation().x, friendlyShip.getLocation().y, null);
					} else {
						g.drawImage(friendlyShipImage, friendlyShip.getLocation().x, friendlyShip.getLocation().y, null);
					}
				}
				Iterator<EnemyShip> enemyShipsIterator = controler.getEnemyShipsOnBattleField().iterator();
				while(enemyShipsIterator.hasNext()){
					EnemyShip enemyShip = enemyShipsIterator.next();
					g.drawImage(enemyShipImage, enemyShip.getLocation().x, enemyShip.getLocation().y, null);
				}
			} catch(Exception e){}
	}
	
	/*if something changes in the gameloop, the view gets repainted.
	 Switch the string if there is one and do the corresponding action.
	 i use threading to play sounds, so the game doesn;t lag when it needs to play a sound for the first time.*/
	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1!=null){
			String command = (String) arg1;
			switch(command){
			case "playShootSound":
				new Thread(new Runnable(){
					@Override
					public void run() {
					playSound(shootSound);
					}
				}).start();
				break;
			case "playExplosionSound":
				new Thread(new Runnable(){
					@Override
					public void run() {
					playSound(explosionSound);
					}
				}).start();
				break;
			case "showNextLevel":
				mainFrame.showNextLevelSwitchPanel();
				break;
			case "defeated":
				defeatedLabel.setVisible(true);
				break;
			case "resetLevel":
				defeatedLabel.setVisible(false);
				break;
			case "gameCompleted":
				mainFrame.showGameCompletedPanel();
				break;
			}
		}
		repaint();
	}
	
	/*called from the method above to play the sound.
	 * One sound is played at a time. But the explosionSound always overrites the shootsound.
	 * Else if the previous sound is still being played.*/
	private void playSound(File soundFile){
		if(clip!=null && clip.isRunning() && soundFile.equals(shootSound)){
			return;
		}
		if(soundFile!=null){
			Clip newClip = null;
			try {
				newClip = AudioSystem.getClip();
					newClip.open(AudioSystem.getAudioInputStream(soundFile));
			} catch (LineUnavailableException | IOException
					| UnsupportedAudioFileException e) {}
			if(newClip!=null){
				clip = newClip;
				clip.start();
				
			}
		}
	}
}
