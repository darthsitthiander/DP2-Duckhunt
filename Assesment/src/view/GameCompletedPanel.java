package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Projectile;

import controler.GameControler;

@SuppressWarnings("serial")
public class GameCompletedPanel extends JPanel implements Observer {
	private MainFrame mainFrame;
	private GameControler controler;
	/*Set instancevariables, preferredsize, labels, and actionlistener.*/
	public GameCompletedPanel(MainFrame newMainFrame, GameControler newControler){
		mainFrame = newMainFrame;
		controler = newControler;
		setPreferredSize(new Dimension(500,630));
		setBackground(Color.gray);

		JLabel titleLabel = new JLabel("<html>Congratiolations! You have completed the game.</html>");
		titleLabel.setPreferredSize(new Dimension(500, 400));
		titleLabel.setFont(new Font("Ariel", Font.BOLD, 50));
		titleLabel.setForeground(Color.red);
		
		JLabel textLabel = new JLabel("<html>Press enter to return to the titlescreen.</html>");
		textLabel.setPreferredSize(new Dimension(500, 230));
		textLabel.setFont(new Font("Ariel", Font.BOLD, 40));
		textLabel.setForeground(Color.blue);
		
		addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent evt){
				if(evt.getKeyCode() == KeyEvent.VK_ENTER){
					mainFrame.showTitleScreen();
				}
			}
		});
		
		add(titleLabel);
		add(textLabel);
	}
	
	//paint the background.
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		try{
			Projectile moon = controler.getBackgroundMoonFromBattleField();
			if(moon != null){
				g.setColor(moon.getProjectileColor());
				g.fillOval(moon.x, moon.y, moon.width, moon.height);
			}
			for(Projectile star:controler.getBackgroundStarsFromBattleField()){
				g.setColor(star.getProjectileColor());
				g.fillRect(star.x, star.y, star.width, star.height);
			}
		}catch(Exception e){}
	}
	
	//On a update from the background, repaint.
	@Override
	public void update(Observable arg0, Object arg1) {
		repaint();
	}

}
