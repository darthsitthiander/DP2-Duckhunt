package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

import controler.GameControler;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	private GameControler controler;
	private GamePanel gamePanel;
	private WelcomePanel welcomePanel;
	/*Set title, controler, unresizable and closeOperation.*/
	public MainFrame(GameControler newControler){
		setTitle("Space Wars");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		controler = newControler;
	}
	
	/*Called from the controler to intialize the welcomepanel, and make the mainframe ready to get visible
	 * and set it visible. then tell the welcome panel to start loading the levels.*/
	public void buildAndShowGui(){
		welcomePanel = new WelcomePanel(this, controler);
		controler.addObserver(welcomePanel);
		setContentPane(welcomePanel);
		pack();
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenDimension.width / 2) - (getSize().width / 2), (screenDimension.height / 2) - (getSize().height / 2));
		setVisible(true);
		
		welcomePanel.startLoading();
	}
	/*The welcome panel sets the gamepanel after the data has loaded for the first time.*/
	public void setGamePanel(GamePanel newGamePanel){
		gamePanel = newGamePanel;
	}
	
	/*Used by the controler to get the gamePanel, then the gameControler to tell him to 
	 * set his buttons ready for the next level.*/
	public GamePanel getGamePanel(){
		return gamePanel;
	}
	
	/*Called from the welcomepanel after the levels are loaded and the player presses enter.
	 * it sets the gamepanel to the content pane and stops the welcome animation.*/
	public void startGame(){
		gamePanel.getGameControlPanel().prepaireNextLevel();
		setContentPane(gamePanel);
		revalidate();
		controler.stopWelcomeScreenAnimation();
	}
	
	/*Called from controler after a level has been completed and there is a next level to swith to the levelswith panel,
	 * add him as a observer, switch the contentpane and give him the focus for the keylistner*/
	public void showNextLevelSwitchPanel(){
		LevelSwitchPanel levelSwitchPanel = new LevelSwitchPanel(this, controler);
		controler.addObserver(levelSwitchPanel);
		setContentPane(levelSwitchPanel);
		levelSwitchPanel.requestFocusInWindow();
		revalidate();
	}
	
	/*Same as above, but called when there are no more levels.*/
	public void showGameCompletedPanel(){
			GameCompletedPanel gameCompletedPanel = new GameCompletedPanel(this, controler);
			controler.addObserver(gameCompletedPanel);
			setContentPane(gameCompletedPanel);
			gameCompletedPanel.requestFocusInWindow();
			revalidate();
	}
	
	/*Called from the gameCompletedpanel when the player presses enter to go back to the welcomepanel.
	 * It tells the welcomepanel to start loading so it there are new levels added in the database, they are loaded again.*/
	public void showTitleScreen(){
		controler.startWelcomeScreenAnimation();
		welcomePanel.prepaireReloadingLevels();
		setContentPane(welcomePanel);
		welcomePanel.requestFocusInWindow();
		revalidate();
		
		welcomePanel.startLoading();
	}
}
