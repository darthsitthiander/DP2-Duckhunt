package model;

import java.awt.Color; 
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import controler.GameControler;

public class BackgroundAnimation {
	
	private ArrayList<Projectile> stars;
	private Projectile moon;
	private Random randomer;
	private GameControler controler;
	private int gameSpeed;
	/*the background animation is the first model that gets intialized. It loops conitnuesly fro the start of the application
	 until you close the application. The speed of the background is equal to the gamespeed. By intializing
	 the gamespeed is set to default(30), because there is no battlefield yet to get the gamespeed from.
	 If the gamespeed changes, then it also gets updated here.*/
	public BackgroundAnimation(GameControler newControler){
		controler = newControler;
		stars = new ArrayList<Projectile>();
		randomer = new Random();
		//loads 300 stars, so when the user opens the application, stars are already visible.
		for(int i = 0; i < 300; i++){
			stars.add(new Projectile(randomer.nextInt(500), randomer.nextInt(680),  2, 5, new Point.Double(0,1), Color.white));
		}
		gameSpeed = 30;
		Thread updateBackgroundThread = new Thread(new UpdateBackground());
		updateBackgroundThread.start();
	}
	
	private class UpdateBackground implements Runnable {
		@Override
		public void run() {
			while(true){
				try{
					Iterator<Projectile> iterator = stars.iterator();
					while(iterator.hasNext()){
						Projectile star = iterator.next();
						//if the star gets out of the screen, then we change the coordinates to a random point above the screen.
						if(star.moveAndmustBeDeleted()){
							star.setBounds(randomer.nextInt(500), (randomer.nextInt(10)+5 * -1), star.width, star.height);
						}
					}
					if(moon!=null){
						if(moon.moveAndmustBeDeleted()){
							moon = null;
						}
					}
					/*There is a change that the moon gets displayed, only when not already visible.*/
					if(randomer.nextInt(1000) > 995 && moon==null){
						moon = new Projectile((randomer.nextInt(500)-50), -100, 100, 100, new Point.Double(0,0.2), new Color(254, 252, 215));
					}
					//tell the controler to update the views.
					controler.updateFromModelToView();
					Thread.sleep(gameSpeed);
				} catch(Exception e){}
			}
		}	
	}
	
	//set gamespeed when it changes.
	public void setGameSpeed(int newGameSpeed){
		gameSpeed = newGameSpeed;
	}
	
	//return the stars and moon so they an be drawn as background in the GameView, LevelSwitch, Welcome and GameCOmpleted panel.
	public ArrayList<Projectile> getBackgroundStars(){
		return stars;
	}
	
	public Projectile getBackgroundMoon(){
		return moon;	
	}

}
