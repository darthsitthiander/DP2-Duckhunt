package model;

import java.awt.Point;
import java.awt.Rectangle;

@SuppressWarnings("serial")
public abstract class GameObject extends Rectangle {
	//save the direction because the ships and projectiles need it.
	protected Point.Double direction;
	// extands rectangle to make it easy to use intersects to check if a bullit hits a target,
	// and to check if the user clicked on one of his own ships.
	public GameObject(int x, int y, int width, int height, Point.Double newDirection){
		setBounds(x, y, width, height);
		direction = newDirection;
	}
}
