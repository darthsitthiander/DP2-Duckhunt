package model;

import java.awt.Color;
import java.awt.Point;

@SuppressWarnings("serial")
public class Projectile extends GameObject {
	protected int speed;
	protected long timeShot;
	private Color color;
	protected Point.Double previousMovement;
	protected boolean isBackgroundProjectile;
	/*speed of projectile set to 2, save the moment the projectile got fired, 
	 * and set backgroundprojectile default to false, or true f the other contrustor gets used. */
	public Projectile(int x, int y, int width, int height, Point.Double direction, Color newColor, long newTimeShot) {
		super(x, y, width, height, direction);
		color = newColor;
		timeShot = newTimeShot;
		speed = 2;
		previousMovement = new Point.Double(0,0);
		isBackgroundProjectile = false;
	}
	
	/*this constructor is used for the BackgroundAnimation. The projectiles wounld't be deleted of an countOut.*/
	public Projectile(int x, int y, int width, int height, Point.Double direction, Color newColor) {
		this(x, y, width, height, direction, newColor, 0);
		isBackgroundProjectile = true;
	}
	
	/*Return the color, called to paint the proectile.*/
	public Color getProjectileColor(){
		return color;
	}
	
	public boolean moveAndmustBeDeleted() {
		Point newCoordinates = new Point(x,y);
		previousMovement.x = previousMovement.x + (direction.x * speed);
		if(previousMovement.x > 1 || previousMovement.x < -1){
			newCoordinates.x = newCoordinates.x + (int)previousMovement.x;
			previousMovement.x = previousMovement.x %1;
		}
		previousMovement.y = previousMovement.y + (direction.y * speed);
		if(previousMovement.y > 1 || previousMovement.y < -1){
			newCoordinates.y = newCoordinates.y + (int)previousMovement.y;
			previousMovement.y = previousMovement.y %1;
		}
		setBounds(newCoordinates.x, newCoordinates.y, width, height);
		/*Shot returns true and that means it needs to be deleted out of the battlefield shots arraylist.
		 it return true if its out of the field or 5 seconds have passed since fired.
		 when its used as background, then it only gets deleted when it gets more than 100 ox oout of the screen,
		 because the moon is 100,100 px and can be shown half on the screen.*/
		if(!isBackgroundProjectile){
			if(x > 500 || x < 0 || y > 500 || y < 0 || timeShot+5000 < System.currentTimeMillis()){
				return true;
			}
		} else {
			if(x > 600 || x < -100 || y > 630 || y < -100){
				return true;
			}
		}
		return false;
	}

}
