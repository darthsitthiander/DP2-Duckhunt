package model;

import java.awt.Color;
import java.awt.Point;

@SuppressWarnings("serial")
public class EnemyShip extends Ship {
	//enemy ship extends ship, which extends GameObject who extends rectangle.
	public EnemyShip(int x, int y, Point.Double direction) {
		super(x, y, direction, Color.red);
	}
	//Move methode with check that he can't exit the screen.
	@Override
	public void move() {
			setBounds(x+(int)(direction.x * speed), y+(int)(direction.y * speed), width, height);
			//452 = 500(screen width) - 48 (ship width)
			if(x<=0 || x >=452){
				direction.x = direction.x * -1;
			}
			if(y<=0 || y >=452){
				direction.y = direction.y * -1;
			}
	}
}
