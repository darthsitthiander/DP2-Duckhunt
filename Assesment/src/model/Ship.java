package model;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("serial")
public abstract class Ship extends GameObject {
	private long lastTimeShotProjectile, lastTimeShotBouncingProjectile, loadingTimeProjectile, loadingTimeBouncingProjectile;
	protected int speed;
	protected Point.Double previousMovement;
	private Color projectileColor;
	/* set the speed of ships to 3, make sure other ships need to implement the move method and check if he can shoot.
	 * Set the loading time of the projectile and bouncing projectile.*/
	public Ship(int x, int y, Point.Double direction, Color newProjectileColor){
		super(x, y, 48, 48, direction);
		speed = 3;
		projectileColor = newProjectileColor;
		previousMovement = new Point.Double(0,0);
		loadingTimeProjectile = 2000;
		loadingTimeBouncingProjectile = 5000;
	}
	
	public abstract void move();
	
	//return a projectile if he can shoot it.
	public Projectile shootProjectile(Point p, long currentTime){
		Projectile returnProjectile = null;
		if(lastTimeShotProjectile+loadingTimeProjectile < currentTime){
			lastTimeShotProjectile = currentTime;
			double dx = p.x - (getX()+24);
			double dy = p.y - (getY()+24);
			double distance = Math.sqrt(dx * dx + dy * dy);
			Point.Double movement = new Point.Double(0,0);
			movement.x = dx / distance;
			movement.y = dy / distance;
			returnProjectile = new Projectile(getLocation().x+19, getLocation().y+19, 10, 10, movement, projectileColor, currentTime);
		}
		return returnProjectile;
	}
	
	//same as method above, but returns a bouncing projectile.
	public BouncingProjectile shootBouncingProjectile(Point p, long currentTime){
		BouncingProjectile returnBouncingProjectile = null;
		if(lastTimeShotBouncingProjectile+loadingTimeBouncingProjectile < currentTime){
			lastTimeShotBouncingProjectile = currentTime;
			double dx = p.x - (getX()+24);
			double dy = p.y - (getY()+24);
			double distance = Math.sqrt(dx * dx + dy * dy);
			Point.Double movement = new Point.Double(0,0);
			movement.x = dx / distance;
			movement.y = dy / distance;
			returnBouncingProjectile = new BouncingProjectile(getLocation().x+14, getLocation().y+14, 20, 20, movement, projectileColor, currentTime);
		}
		return returnBouncingProjectile;
	}
	
	/*method to shoot an return an arraylist of 8 proectiles in 8 directions.
	 * Called when a ship stands on the moon and fires.*/
	public ArrayList<Projectile> shootProjectilesFromMoon(long currentTime){
			ArrayList<Projectile> returnProjectiles = new ArrayList<Projectile>();
			int xDir = -1;
			int yDir = -1;
			for(int i=0; i<8;i++){
				returnProjectiles.add(new Projectile(getLocation().x+19, getLocation().y+19, 10, 10, new Point.Double(xDir,yDir), projectileColor, currentTime));
				if(xDir<=0 && yDir==-1){
					xDir++;
				} else if(xDir==1 && yDir<=0){
					yDir++;
					} else if(xDir>=0 && yDir==1){
					xDir--;
				} else if(xDir==-1 && yDir>=0){
					yDir--;
				}
			}
		return returnProjectiles;
	}
	
	/*returns colored bouncing projectile. called from the welcomescreenanimation.*/
	public BouncingProjectile shootWelcomeScreenBouncingProjectile(long currentTime){
			Random randomer = new Random();
			Point p = new Point(randomer.nextInt(500), randomer.nextInt(630));
			double dx = p.x - (getX()+24);
			double dy = p.y - (getY()+24);
			double distance = Math.sqrt(dx * dx + dy * dy);
			Point.Double movement = new Point.Double(0,0);
			movement.x = dx / distance;
			movement.y = dy / distance;
			
			return new BouncingProjectile(getLocation().x+14, getLocation().y+14, 20, 20, movement, new Color(randomer.nextInt(255), randomer.nextInt(255), randomer.nextInt(255)), currentTime, 630);
	}
	
	/*returns 8 colored bouncing projectiles if ship intersects the moon, used on welcomescreenanimation.*/
	public ArrayList<BouncingProjectile> shootWelcomeScreenBouncingProjectilesFromMoon(long currentTime){
		ArrayList<BouncingProjectile> returnBouncingProjectiles = new ArrayList<BouncingProjectile>();
		int xDir = -1;
		int yDir = -1;
		Random randomer = new Random();
		for(int i=0; i<8;i++){
			returnBouncingProjectiles.add(new BouncingProjectile(getLocation().x+14, getLocation().y+14, 20, 20, new Point.Double(xDir,yDir), new Color(randomer.nextInt(255), randomer.nextInt(255), randomer.nextInt(255)), currentTime, 630));
			if(xDir<=0 && yDir==-1){
				xDir++;
			} else if(xDir==1 && yDir<=0){
				yDir++;
				} else if(xDir>=0 && yDir==1){
				xDir--;
			} else if(xDir==-1 && yDir>=0){
				yDir--;
			}
		}
	return returnBouncingProjectiles;
}
	
	//geters fpr the lastshot and loading times. Used in gamecontrolerpanel to draw the projectile loadingbars.
	public long getLastTimeShotProjectile(){
		return lastTimeShotProjectile;
	}
	
	public long getLastTimeShotBouncingProjectile(){
		return lastTimeShotBouncingProjectile;
	}
	
	public long getLoadingTimeProjectile(){
		return loadingTimeProjectile;
	}
	
	public long getLoadingTimeBouncingProjectile(){
		return loadingTimeBouncingProjectile;
	}
}
