package model;

import java.awt.Color;
import java.awt.Point;

@SuppressWarnings("serial")
public class BouncingProjectile extends Projectile {
	private boolean bouncingIn, bouncing, outBounced;
	private int originalWidth, originalHeight, bounceDirection, lastBounceDirection, counter, numOfBounces, screenHeight;
	/* intialize variables and send data to the super(Projectile) class.
	to use the projectiles on the welcomescreen , the screenheight is different, so 2 contructors are used. On the gameViewPanel
	 the default constructor can still be used, and the screenHeight is set to default(500);*/
	public BouncingProjectile(int x, int y, int width, int height,
			Point.Double direction, Color newColor,
			long timeShot, int newScreenHeight) {
		super(x, y, width, height, direction, newColor, timeShot);
		bouncing = false;
		bouncingIn = false;
		outBounced = false;
		originalWidth  = width;
		originalHeight = height;
		bounceDirection = 0;
		counter = 0;
		numOfBounces = 0;
		screenHeight = newScreenHeight;
	}
	public BouncingProjectile(int x, int y, int width, int height,
			Point.Double direction, Color newColor,
			long timeShot) {
		this(x, y, width, height, direction, newColor, timeShot, 500);
	}
	
	/*the big bounce method. If there is no bounce going on, then it just moves like the regular projectile.
	 * After the movement, it checks if it can bounce. First check if it can bounce in a corner 
	 * (from left upper clockwise to left under corner)
	 * and check if it bounces against a wall (1=north wall, 2=east wall etc.)
	 * It returns false if it needs to be deleted.*/
	public boolean moveAndmustBeDeleted() {
		Point newCoordinates = new Point(x,y);
		if(bounceDirection == 0){
			previousMovement.x = previousMovement.x + (direction.x * speed);
			if(previousMovement.x > 1 || previousMovement.x < -1){
				newCoordinates.x = newCoordinates.x + (int)previousMovement.x;
				previousMovement.x = previousMovement.x %1;
			}
			previousMovement.y = previousMovement.y + (direction.y * speed);
			if(previousMovement.y > 1 || previousMovement.y < -1){
				newCoordinates.y = newCoordinates.y + (int)previousMovement.y;
				previousMovement.y = previousMovement.y %1;
			}
			setBounds(newCoordinates.x, newCoordinates.y, width, height);
			if(!outBounced){
				if(newCoordinates.x >= 480 || newCoordinates.x <= 0 || newCoordinates.y >= (screenHeight-20) || newCoordinates.y <= 0){
				//check if the lastBounce isn't this bounce, else the ball will be stuck because he bounces and bounces over and over.
					if(y <= 0 && x <= 0 && lastBounceDirection!=5){
						bounceDirection = 5;
						lastBounceDirection = 5;
						bouncingIn = true;
						bouncing = true;
					} else if(y <= 0 && x >= 480 && lastBounceDirection!=6){
						bounceDirection = 6;
						lastBounceDirection = 6;
						bouncingIn = true;
						bouncing = true;
					} else if(y >= (screenHeight-20) && x >= 480 && lastBounceDirection!=7){
						bounceDirection = 7;
						lastBounceDirection = 7;
						bouncingIn = true;
						bouncing = true;
					} else if(y >= (screenHeight-20) && x <= 0 && lastBounceDirection!=8){
						bounceDirection = 8;
						lastBounceDirection = 8;
						bouncingIn = true;
						bouncing = true;
					}else if(y <= 0 && lastBounceDirection!=1 && lastBounceDirection!=5  && lastBounceDirection!=6){
						bounceDirection = 1;
						lastBounceDirection = 1;
						bouncingIn = true;
						bouncing = true;
					} else if(x >= 480 && lastBounceDirection!=2 && lastBounceDirection!=6 && lastBounceDirection!=7){
						bounceDirection = 2;
						lastBounceDirection = 2;
						bouncingIn = true;
						bouncing = true;
					} else if(y >= (screenHeight-20) && lastBounceDirection!=3 && lastBounceDirection!=7 && lastBounceDirection!=8){
						bounceDirection = 3;
						lastBounceDirection = 3;
						bouncingIn = true;
						bouncing = true;
					} else if(x <= 0 && lastBounceDirection!=4 && lastBounceDirection!=5 && lastBounceDirection!=8){
						bounceDirection = 4;
						lastBounceDirection = 4;
						bouncingIn = true;
						bouncing = true;
					}
				}
			}
			//the projectile may bounce 3 times, then when it leaves the screen it gets deleted.
		} else if(!outBounced){
			/*Then the bounce gets preformend.
			 * First check in which direction the projectile is bouncing, increase/decrease size and position.
			 * The counter is necceserly becuse its positionneeds to be the changed the half of the increase of the size
			 * to keep the projectile on his place. And this for every bounce option.*/
			switch(bounceDirection){
				case 1:
					if(bouncingIn){
						height--;
						width++;
						counter++;
						if(counter >= 2){
							x--;
							counter = 0;
						}
						if(height<=(originalHeight / 4)){
							numOfBounces++;
							bouncingIn = false;
							counter = 0;
						}
					} else {
						height++;
						width--;
						counter++;
						if(counter >= 2){
							x++;
							counter = 0;
						}
						if(height >= originalHeight){
							height = originalHeight;
							width = originalWidth;
							bouncing = false;
							counter = 0;
						}
					}
				break;
			case 2:
				if(bouncingIn){
					width--;
					height++;
					x++;
					counter++;
					if(counter >= 2){
						y--;
						counter = 0;
					}
					if(width <= (originalWidth / 4)){
						numOfBounces++;
						bouncingIn = false;
						counter = 0;
					}
				} else {
					width++;
					height--;
					x--;
					counter++;
					if(counter >= 2){
						y++;
						counter = 0;
					}
					if(width >= originalWidth){
						width = originalWidth;
						height  = originalHeight;
						bouncing = false;
						counter = 0;
					}
				}
				break;
			case 3:
				if(bouncingIn){
					height--;
					width++;
					y++;
					counter++;
					if(counter >= 2){
						x--;
						counter = 0;
					}
					if(height<=(originalHeight / 4)){
						numOfBounces++;
						bouncingIn = false;
						counter = 0;
					}
				} else {
					height++;
					width--;
					y--;
					counter++;
					if(counter >= 2){
						x++;
						counter = 0;
					}
					if(height >= originalHeight){
						height = originalHeight;
						width  = originalWidth;
						bouncing = false;
						counter = 0;
					}
				}
				break;
			case 4:
				if(bouncingIn){
					width--;
					height++;
					counter++;
					if(counter >= 2){
						y--;
						counter++;
					}
					if(width<=(originalWidth / 4)){
						numOfBounces++;
						bouncingIn = false;
						counter = 0;
					}
				} else {
					width++;
					height--;
					counter++;
					if(counter >= 2){
						y++;
						counter = 0;
					}
					if(width>=originalWidth){
						width = originalWidth;
						height = originalHeight;
						bouncing = false;
						counter = 0;
					}
				}
				break;
			case 5:
				if(bouncingIn){
					width--;
					height--;
					if(width <= (originalWidth / 4) || height <= (originalHeight / 4)){
						numOfBounces++;
						bouncingIn = false;
					}
				} else {
					width++;
					height++;
					if(width >= originalWidth || height >= originalHeight){
						width = originalWidth;
						height = originalHeight;
						bouncing = false;
					}
				}
				break;
			case 6:
				if(bouncingIn){
					width--;
					height--;
					x++;
					if(width <= (originalWidth / 4) || height <= (originalHeight / 4)){
						numOfBounces++;
						bouncingIn = false;
					}
				} else {
					width++;
					height++;
					x--;
					if(width >= originalWidth || height >= originalHeight){
						width = originalWidth;
						height = originalHeight;
						bouncing = false;
					}
				}
				break;
			case 7:
				if(bouncingIn){
					width--;
					height--;
					x++;
					y++;
					if(width <= (originalWidth / 4) || height <= (originalHeight / 4)){
						numOfBounces++;
						bouncingIn = false;
					}
				} else {
					width++;
					height++;
					x--;
					y--;
					if(width >= originalWidth || height >= originalHeight){
						width = originalWidth;
						height = originalHeight;
						bouncing = false;
					}
				}
				break;
			case 8:
				if(bouncingIn){
					width--;
					height--;
					y++;
					if(width <= (originalWidth / 4) || height <= (originalHeight / 4)){
						numOfBounces++;
						bouncingIn = false;
					}
				} else {
					width++;
					height++;
					y--;
					if(width >= originalWidth || height >= originalHeight){
						width = originalWidth;
						height = originalHeight;
						bouncing = false;
					}
				}
				break;
			}
			//if the projectile has stopped bouncing, then the direction gets turned around depending on the bounce location
			// and is set back to 0 bounceDirection.
			if(!bouncing){
				if(bounceDirection == 2 || bounceDirection >= 4){
					direction.x = direction.x * -1;
				} 
				if(bounceDirection == 1 || bounceDirection == 3 || bounceDirection >= 5) {
					direction.y = direction.y * -1;
				}
				bounceDirection = 0;
				if(numOfBounces >= 3){
					outBounced = true;
				}
			}
		}
		if(screenHeight==500 && timeShot + 10000 < System.currentTimeMillis()
				|| screenHeight == 580 && timeShot + 20000 < System.currentTimeMillis()){
			return true;
		}
		if(outBounced){
			if(x < -20 || x > 520 || y < -20 || y > screenHeight+20){
				return true;
			}
		}
		return false;
	}

}
