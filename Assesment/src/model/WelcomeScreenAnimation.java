package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import controler.GameControler;

public class WelcomeScreenAnimation {
	
	private ArrayList<BouncingProjectile> bouncingProjectiles;
	private FriendlyShip friendlyShip;
	private long previouslyMovedTime, previouslyShotTime;
	private GameControler controler;
	private Random randomer;
	/*Intialise instance variables and start the looping thread.*/
	public WelcomeScreenAnimation(GameControler newControler){
		controler = newControler;
		randomer = new Random();
		friendlyShip = new FriendlyShip(200, 200);
		bouncingProjectiles = new ArrayList<BouncingProjectile>();
		previouslyMovedTime = System.currentTimeMillis();
		previouslyShotTime = System.currentTimeMillis();
		Thread animationThread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(true){
						long currentTime = System.currentTimeMillis();
						updateFriendlyShip(currentTime);
						updateBouncingProjectiles(currentTime);
						controler.updateFromModelToView();
						try {
							Thread.sleep(30);
						} catch (InterruptedException e) {}
				}
			}
		});
		animationThread.start();
	}
	
	/*Move the friendlyship and set a new random location to move to every 3 seconds.*/
	private void updateFriendlyShip(long currentTime){
		if(previouslyMovedTime + 3000 < currentTime){
			friendlyShip.setTarget(new Point(randomer.nextInt(500), randomer.nextInt(630)));
			previouslyMovedTime = currentTime;
		}
		friendlyShip.move();
	}
	
	/*intialize the bouncingprojectiles to move randomly in any direction with a random color.
	 * Also move them and delete them when needed.*/
	private void updateBouncingProjectiles(long currentTime){
		if(previouslyShotTime + 1500 < currentTime){
			if(controler.getBackgroundMoonFromBattleField()!=null && friendlyShip.intersects(controler.getBackgroundMoonFromBattleField())){
				for(BouncingProjectile bouncingProjectile:friendlyShip.shootWelcomeScreenBouncingProjectilesFromMoon(currentTime)){
					bouncingProjectiles.add(bouncingProjectile);
				}
			} else {
				Point.Double randomPoint = new Point.Double(randomer.nextFloat(),randomer.nextFloat());
				if(randomer.nextBoolean()){
					randomPoint.x = randomPoint.x * -1;
					randomPoint.y = randomPoint.y * -1;
				}
				bouncingProjectiles.add(friendlyShip.shootWelcomeScreenBouncingProjectile(currentTime));
			}
			previouslyShotTime = currentTime;
		}
		Iterator<BouncingProjectile> bouncingProjectilesIterator = bouncingProjectiles.iterator();
		while(bouncingProjectilesIterator.hasNext()){
			BouncingProjectile shot = bouncingProjectilesIterator.next();
			if(shot.moveAndmustBeDeleted()){
				bouncingProjectilesIterator.remove();
			}
		}
	}
	
	//return the friendlyship and bouncing projectiles so it can be drawn on the welcomepanel.
	public FriendlyShip getFriendlyShip(){
		return friendlyShip;
	}
	
	public ArrayList<BouncingProjectile> getBouncingProjectiles(){
		return bouncingProjectiles;
	}
}
