package model;

import java.awt.Color;  
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import controler.GameControler;


public class BattleField {
	
	private GameControler controler;
	private ArrayList<FriendlyShip> friendlyShipsOnBattleField;
	private ArrayList<EnemyShip> enemyShipsOnBattleField;
	private ArrayList<Projectile> projectilesOnBattleField;
	private ArrayList<String> levelsData;
	private FriendlyShip selectedShip;
	private boolean gameIsPlaying;
	private int gameSpeed;
	private int level, maxLevel;
	/*set the controler, default game speed and create the arraylists,
	 * set level and maxLevel. */
	public BattleField(GameControler newControler){
		controler = newControler;
		gameSpeed = 30;
		gameIsPlaying = false;
		friendlyShipsOnBattleField = new ArrayList<FriendlyShip>();
		enemyShipsOnBattleField = new ArrayList<EnemyShip>();
		projectilesOnBattleField = new ArrayList<Projectile>();
		level = -1;
		maxLevel = 0;
	}
	
	//gets called when the welcomepanel is visible so it can start loading the levels.
	public void loadLevelData() {
		Thread loadData = new Thread(new DataLoader());
		loadData.start();
	}
	
	private class DataLoader implements Runnable {
		private Connection m_con;

		@Override
		public void run() {
			//create a local arrayList to store the data of the levels.
			ArrayList<String> levelsData = new ArrayList<String>();
			/*First, load the data from the documents in the resources map. Loop depending on the
			 * number of levels inside the NumberOfLevels document. So you can create new levels without changing the code.*/
			try {
				for (int i = 1; i <= Integer.parseInt(readFileLines(new File("resources/NumberOfLevels"))); i++) {
					//in case file doesn;t exist anymore, a try, catch. Nothing gets added when something goes wrong.
					try {
						levelsData.add(readFileLines(new File("resources/Level" + i)));
						try {
							//Take some rest so the loading time in creases so 'loading' is visible.
							Thread.sleep(200);
						} catch (InterruptedException e) {}
					} catch (IOException e) {}
				}
			} catch (NumberFormatException | IOException e) {}
			//now load the levels out of the database. A Try catch just in case the database seems to be offline.
			try {
				Class.forName("com.mysql.jdbc.Driver");
				m_con = (Connection) DriverManager
						.getConnection( "jdbc:mysql://databases.aii.avans.nl:3306/mjwspren_db2",
								"mjwspren", "Ab12345");
				Statement selectStatement = (Statement) m_con.createStatement();
				ResultSet databaseLevels = selectStatement
						.executeQuery("SELECT * FROM javaassesment ORDER BY db_level, color, xPos, yPos, xDirection, yDirection");
				int currentLevel = -1;
				String levelData = "";
				/*Loop trhough all the results. Check the db_level, if its equal to previous, then add data to llevelData string,
				 * else add previous string to the levelsData arrayList en clear the string and set the new previous level.*/
				while (databaseLevels.next()) {
					if (databaseLevels.getInt("db_level") != currentLevel) {
						if (currentLevel != -1) {
							levelsData.add(levelData);
							levelData = "";
						}
						currentLevel = databaseLevels.getInt("db_level");
					}
					levelData += databaseLevels.getString("color") + ","
							+ databaseLevels.getInt("xPos") + ","
							+ databaseLevels.getInt("yPos") + ","
							+ databaseLevels.getInt("xDirection") + ","
							+ databaseLevels.getInt("yDirection") + ";";
					// and wait to increase loadingtime again.
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {}
				}
				// add the last leveldata of the database into the local ArrayList.
				levelsData.add(levelData);
			} catch (Exception e) {}
			//call the method to set the loaded data, and tell the welcmepanel that the loading is completed.
			setLoadedLevelData(levelsData);
			controler.updateFromModelToView("levelsLoaded");
		}
		
		//Method called while for reading the data out of the files.
		private String readFileLines(File f) throws IOException {
			Path path = f.toPath();
			String contentString = "";
			List<String> readedLines = Files.readAllLines(path,
					Charset.defaultCharset());
			for (String s : readedLines) {
				contentString += s;
			}
			return contentString;
		}
	}
	
	/* set the data after the levels are loaded and loadLevel 1. 
	 * If there is no data, load a default level.*/
	private void setLoadedLevelData(ArrayList<String> newLevelsData) {
		level = 1;
		levelsData = newLevelsData;
		maxLevel = levelsData.size();
		if (maxLevel > 0) {
			loadLevel(levelsData.get(0), 1);
		} else {
			loadLevel("", 1);
		}
	}
	
	private void loadLevel(String levelData, int newLevel) {
		/* intialaze player- and enemy ships. if nothings gets loaded from the
		 * files and the database, then a default level is loaded.
		 * 
		 * There gets checked if there is at least one friendlyShip and a enemyShip. If not, or any of the ships
		 * creates an error/contains invalid data, then the whole level doesn;t get loaded.*/
		level = newLevel;
		boolean loadedFilesCorrectly = false;
		boolean friendlyShipAdded = false;
		boolean enemyShipAdded = false;
		boolean posAndDirAreValid = true;
		if (levelData != "" || levelData != null) {
			try {
				String[] ships = levelData.split(";");
				for (String ship : ships) {
					String[] shipData = ship.split(",");
					int xPos = Integer.parseInt(shipData[1]);
					int yPos = Integer.parseInt(shipData[2]);
					/*Check if the coordinates are within the screen, else this level is loaded incorrectly*/
					if(xPos < 0 || xPos > 452 || yPos < 0 || yPos > 452){
						posAndDirAreValid = false;
					}
					if (shipData[0].equals("green")) {
						friendlyShipsOnBattleField.add(new FriendlyShip(
								xPos, yPos));
						friendlyShipAdded = true;
					} else {
						int xDirection = Integer.parseInt(shipData[3]);
						int yDirection = Integer.parseInt(shipData[4]);
						/*Check posiion and direciton of enemyship. he is only allowed to start to a wall or corner,
						 * if he is goign into the opposide direction.*/
						if(xDirection < -1 || xDirection > 1 || yDirection < -1 || yDirection > 1
								|| xPos < 0 || xPos == 0 && xDirection == -1 || xPos > 452 || xPos == 452 && xDirection == 1
								|| yPos < 0 || yPos == 0 && yDirection == -1 || yPos > 452 || yPos == 452 && yDirection == 1){
							posAndDirAreValid = false;
						}
						enemyShipsOnBattleField.add(new EnemyShip(xPos,yPos,new Point.Double(xDirection, yDirection)));
						enemyShipAdded = true;
					}
				}
				if(friendlyShipAdded && enemyShipAdded && posAndDirAreValid){
					loadedFilesCorrectly = true;
				} else {
					/*Clear the added ships if the one of the ships are incorrect*/
					friendlyShipsOnBattleField.clear();
					enemyShipsOnBattleField.clear();
				}
			} catch (Exception e) {
			}
		}
		if (!loadedFilesCorrectly) {
			friendlyShipsOnBattleField.add(new FriendlyShip(100, 400));
			friendlyShipsOnBattleField.add(new FriendlyShip(300, 400));

			enemyShipsOnBattleField.add(new EnemyShip(200, 100, new Point.Double(1, 0)));
			enemyShipsOnBattleField.add(new EnemyShip(400, 100, new Point.Double(0, 1)));
		}
	}
	
	/*Called from GameControler when the player presses the Reset level button. Checks if maxLevel is bigger or equal to 
	 * the current level. Else, then it eans the maxLevel is 0 becouse there is no valid levelData, the default level gets load.*/
	public void resetLevel(){
		clearBattleField();
		if (maxLevel >= level) {
			loadLevel(levelsData.get(level - 1), level);
		} else {
			loadLevel("", level);
		}
	}
	
	/*Load the next level and inreament the level variable.*/
	public void loadNextLevel(){
		loadLevel(levelsData.get(level), ++level);
		// sleep a little so it takes longer to 'load' a level and you can see
		// the loading.
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {}
	}
	
	/*getter to get the max Level. Used by the controler to check if he needs to sow the changelevelspanel and 
	 * load the next level,or show the gameCOmpleted panel.*/
	public int getMaxLevel(){
		return maxLevel;
	}
	
	private class GameLoop implements Runnable{
		@Override
		public void run() {
			while(gameIsPlaying){
					long currentTime = System.currentTimeMillis();
					updateProjectiles(currentTime);
					updateShips(currentTime);
					controler.updateFromModelToView();
					try {
						Thread.sleep(gameSpeed);
				} catch (InterruptedException e) {}
			}
		}
	}
	
	public void clearBattleField(){
		gameIsPlaying = false;
		selectedShip = null;
		friendlyShipsOnBattleField.clear();
		enemyShipsOnBattleField.clear();
		projectilesOnBattleField.clear();
	}
	
	//getters for the ships and projectiles
	public ArrayList<FriendlyShip> getFriendlyShipsOnBattleField(){
		return friendlyShipsOnBattleField;
	}
	
	public ArrayList<EnemyShip> getEnemyShipsOnBattleField(){
		return enemyShipsOnBattleField;
	}
	
	public ArrayList<Projectile> getProjectilesOnBattleField(){
		return projectilesOnBattleField;
	}
	
	//getter and setter for the selected friendship
	public void setSelectedShip(FriendlyShip newSelectedShip){
		selectedShip = newSelectedShip;
	}
	
	public FriendlyShip getSelectedShip(){
		return selectedShip;
	}
	
	//set gameIsRunnign true so the thread can loop, and start the thread.
	public void startGame(){
		gameIsPlaying = true;
		new Thread(new GameLoop()).start();
	}
	
	//pause the game / stop the gameLoop thread.
	public void pauseGame(){
		gameIsPlaying = false;
	}
	
	//gameloop method to update the projectiles.
	private void updateProjectiles(long currentTime){
		/*for each projectile check if it intersects a ship and if it is an enemyship(with means the color of the bulit is red to hit a friendlyShip
		and green to hit an enemyShip) then that ship and bullit gets deleted and the controler gets the order to tell the view to play the explosion sound.*/
		Iterator<Projectile> projectilesIterator = projectilesOnBattleField.iterator();
		while(projectilesIterator.hasNext()){
			Projectile shot = projectilesIterator.next();
			Iterator<FriendlyShip> friendlyShipsIterator = friendlyShipsOnBattleField.iterator(); 
			while(friendlyShipsIterator.hasNext()){
				FriendlyShip fShip = friendlyShipsIterator.next();
				if(shot.intersects(fShip) && shot.getProjectileColor().equals(Color.red)){
					//deselect the selected ship when the selectedship gets hit.
					if(selectedShip!=null && selectedShip.equals(fShip)){
						selectedShip = null;
					}
					friendlyShipsIterator.remove();
					projectilesIterator.remove();
					controler.playExplosionSound();
					if(friendlyShipsOnBattleField.size() == 0 && gameIsPlaying){
						gameIsPlaying = false;
						//Notify the GameView that you are defeated.
						controler.showDefeated();
					}
					return;
				}
			}
			Iterator<EnemyShip> enemyShipsIterator = enemyShipsOnBattleField.iterator();
			while(enemyShipsIterator.hasNext()){
				EnemyShip eShip = enemyShipsIterator.next();
				if(shot.intersects(eShip) && shot.getProjectileColor().equals(Color.green)){
					enemyShipsIterator.remove();
					projectilesIterator.remove();
					controler.playExplosionSound();
					if(enemyShipsOnBattleField.size() == 0 && gameIsPlaying){
						gameIsPlaying = false;
						controler.showNextLevelScreen(level+1);
					}
					return;
				}
			}
			/*the moveAndMustBeDeleted moves the projectile and returns true when the projectile gets out of the screen 
			 * or has been in the screen for longer than 5 secs. Else it returns false.*/
			if(shot.moveAndmustBeDeleted()){
				projectilesIterator.remove();
			}
		}
	}
	
	/*let all the enemy and playerships move and check if they are within a range of 150px by 150px of a ship of the enemy,
	If true then a bullit gets fired and a sound gets played.*/
	private void updateShips(long currentTime){
		Iterator<EnemyShip> enemyShipsIterator = enemyShipsOnBattleField.iterator();
		while(enemyShipsIterator.hasNext()){
			EnemyShip enemyShip = enemyShipsIterator.next();
			enemyShip.move();
			Rectangle range = new Rectangle(enemyShip.getLocation().x-126, enemyShip.getLocation().y-126, 300, 300);
			Iterator<FriendlyShip> friendlyShipsIterator = friendlyShipsOnBattleField.iterator();
			while(friendlyShipsIterator.hasNext()){
				FriendlyShip fShip = friendlyShipsIterator.next();
				if(range.intersects(fShip)){
					Projectile localProjectile = enemyShip.shootProjectile(new Point(fShip.getLocation().x+24,fShip.getLocation().y+24), currentTime);
					if(localProjectile!=null){
						//if the ship is shoting from the moon, then 8 shots are fired in 8 directions.
						if(controler.getBackgroundMoonFromBattleField()!=null && enemyShip.intersects(controler.getBackgroundMoonFromBattleField())){
							for(Projectile moonShot:enemyShip.shootProjectilesFromMoon(currentTime)){
								projectilesOnBattleField.add(moonShot);
							}
						}
						projectilesOnBattleField.add(localProjectile);
						controler.playShootSound();
					}
				}
			}
		}
		Iterator<FriendlyShip> friendlyShipsIterator2 = friendlyShipsOnBattleField.iterator();
		while(friendlyShipsIterator2.hasNext()){
			FriendlyShip friendlyShip = friendlyShipsIterator2.next();
			friendlyShip.move();
			Rectangle range = new Rectangle(friendlyShip.getLocation().x-126, friendlyShip.getLocation().y-126, 300, 300);
			Iterator<EnemyShip> enemyShipsIterator2 = enemyShipsOnBattleField.iterator();
			while(enemyShipsIterator2.hasNext()){
				EnemyShip eShip = enemyShipsIterator2.next();
				if(range.intersects(eShip)){
					Projectile localProjectile = friendlyShip.shootProjectile(new Point(eShip.getLocation().x+24,eShip.getLocation().y+24), currentTime);
					if(localProjectile!=null){
						if(controler.getBackgroundMoonFromBattleField()!=null && friendlyShip.intersects(controler.getBackgroundMoonFromBattleField())){
							for(Projectile moonShot:friendlyShip.shootProjectilesFromMoon(currentTime)){
								projectilesOnBattleField.add(moonShot);
							}
						}
						projectilesOnBattleField.add(localProjectile);
						controler.playShootSound();
					}
				}
			}
		}
	}
	
	/*setter and getter for the gamespeed and boolean to check if the game is playing,
	 used to check if an action needs to be preformed when the player clicks somewhere on the gamescreen.
	 When the game isn;t playing, the clicks are useless.*/
	public void setGameSpeed(int newGameSpeed){
		gameSpeed = newGameSpeed;
	}
	
	public int getGameSpeed(){
		return gameSpeed;
	}
	
	public boolean gameIsPlaying(){
		return gameIsPlaying;
	}
	
	/*Called from the controler if the player clicks somewhere on the screen with his right mousebutton,
	 * exhapt on a selectedShip.*/
	public void shootBouncingProjectile(Ship shotingShip, Rectangle mousePosition){
		BouncingProjectile localBouncingProjectile = shotingShip.shootBouncingProjectile(new Point(mousePosition.getLocation().x,mousePosition.getLocation().y), System.currentTimeMillis());
		if(localBouncingProjectile!=null){
			projectilesOnBattleField.add(localBouncingProjectile);
			controler.playShootSound();	
		}
	}
	
	//returns the currentLevel. Used to show the current levle on the levelSwitchScreen.
	public int getCurrentLevel(){
		return level;
	}
}
