package model;

import java.awt.Color;
import java.awt.Point;

@SuppressWarnings("serial")
public class FriendlyShip extends Ship {
	private Point destination;
	private int nSteps;
	//give the super classes the information they need.
	public FriendlyShip(int x, int y) {
		super(x, y, new Point.Double(0,0), Color.green);
		destination = new Point(x, y);
		nSteps = 0;
	}
	/*set the target to move to. Called from the controler after the player clicked on the gameviewpanel.
	it sets the distance/difference between the targetpoint and the center of the ship(which is 48 px width and height).
	Depending on the distense, the direction of the movement is set.
	The destination is set to check if the ship has reached it and can stop moving.*/
	public void setTarget (Point p) {
		double dx = p.x - (getX()+24);
		double dy = p.y - (getY()+24);
		double distance = Math.sqrt(dx * dx + dy * dy);
		direction.x = speed * dx / distance;
		direction.y = speed * dy / distance;
		destination = new Point(p.x-24, p.y-24);
		nSteps = (int) (distance / speed);
	}
	/*If there are steps to take, then the steps will be taken until 0.
	 * the previous move gets saved as long as its below 1. The there wont be a move to that x or y position.
	 * When its above 1, then it will move the number and the decimals will be saved in the previousMovement.
	 * The direction will be set to 0 when the destination has been reached.*/
	@Override
	public void move() {
		if(nSteps > 0){
			Point newCoordinates = new Point(x,y);
			previousMovement.x = previousMovement.x + direction.x;
			if(previousMovement.x > 1 || previousMovement.x < -1){
				newCoordinates.x = newCoordinates.x + (int)previousMovement.x;
				previousMovement.x = previousMovement.x %1;
			}
			previousMovement.y = previousMovement.y + direction.y;
			if(previousMovement.y > 1 || previousMovement.y < -1){
				newCoordinates.y = newCoordinates.y + (int)previousMovement.y;
				previousMovement.y = previousMovement.y %1;
			}
			setBounds(newCoordinates.x, newCoordinates.y, width, height);
			if(direction.x > 0 && x>=destination.x || direction.x < 0 && x<=destination.x || x >= 452 || x <= 0){
				direction.x = 0;
			}
			if(direction.y > 0 && y>=destination.y || direction.y < 0 && y<=destination.y || y >= 452 || y <= 0){
				direction.y = 0;
			}
			nSteps--;
		}
	}
}
