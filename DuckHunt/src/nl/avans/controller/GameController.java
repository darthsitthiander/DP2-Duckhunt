package nl.avans.controller;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import javax.swing.SwingUtilities;
import nl.avans.enums.SpecificAction;
import nl.avans.model.Action;
import nl.avans.model.Game;
import nl.avans.model.LevelMap;
import nl.avans.model.repositories.LevelFactory;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;
import nl.avans.view.MainFrame;

public class GameController extends Observable
{
	public final LevelFactory levelFactory;
	public final UnitFactory unitFactory;
	private MainFrame mainFrame;
	public Game game;

	public GameController ()
	{
		mainFrame 												= new MainFrame(this);
		game													= new Game(new Rectangle(0, 0, 256, 160), new Rectangle(0, 160, 256, 20), this);
        unitFactory												= UnitFactory.getInstance();
        levelFactory											= new LevelFactory(game);

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()									{ mainFrame.buildAndShowGui(); }
		});
	}

	public MouseAdapter getMouseAdapter()
	{
		MouseAdapter mouseAdapter 								= new MouseAdapter()
		{
			@Override
			public void mousePressed (MouseEvent mouseEvent)	{ game.getActionContainer().callSpecificAction(SpecificAction.ADD, new Action(mouseEvent.getX(), mouseEvent.getY())); }
		};
		
		return mouseAdapter;
	}
	
	public void startGame ()
	{
		game.start();
	}
	
	public void updateFromModelToView ()
	{
		setChanged();
		notifyObservers(); // triggers repaint
	}
	
	public Unit getCurrentUnit()								{ return game._currentLevel.getCurrentUnit(); }
	public int getAmountOfShots ()								{ return game.getAmountOfShots(); }
}