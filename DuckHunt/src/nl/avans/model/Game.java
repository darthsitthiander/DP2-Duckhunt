package nl.avans.model;

import java.awt.Rectangle;

import nl.avans.controller.GameController;
import nl.avans.enums.SpecificAction;
import nl.avans.model.levels.BaseLevelState;
import nl.avans.model.repositories.LevelFactory;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.repositories.containers.MoveContainer;
import nl.avans.model.repositories.states.FleeState;
import nl.avans.model.screenparts.units.Unit;

public class Game
{
	public Rectangle shootingArea;
    public Rectangle spawnArea;
	private int score;
	public float deltaTime;
	public Gun gun;
	public boolean isPlaying;
	private GameController gameController;
	private ActionContainer _actionContainer;
    public MoveContainer _moveContainer;
	public BaseLevelState _currentLevel;
	public int fps, fpsCap;
	private float fpsCounter;
    public boolean showFPS;

    public Game(Rectangle shootingArea, Rectangle spawnArea, GameController gameController)
	{
		this.isPlaying = false;
        this.deltaTime 								= 0;
        this.score									= 0;
		this.shootingArea							= shootingArea;
		this.spawnArea								= spawnArea;
		this.gun									= new Gun();
		this.gameController							= gameController;
		this._actionContainer                       = new ActionContainer();
        this._moveContainer                         = new MoveContainer();
		this.fpsCounter								= 0f;
        this.fpsCap                                 = 60;
        this.showFPS                                = true;
	}

	public int getScore ()							{ return this.score; }
	
	public ActionContainer getActionContainer()		{ return this._actionContainer; }

    public void start(){
        isPlaying 									= true;
        startFirstLevel();
        new Thread(new GameLoop()).start();
    }

	public void startFirstLevel() {
		if(getLevelFactory().GetMap().size() == 0)  return;
		this._currentLevel 							= getLevelFactory().getFirstLevel();
	}

	public void setLevel(BaseLevelState level)      {
        this._currentLevel                          = level;
        this.gun.reload();
    }

	public void setScore(int score)                 { this.score = score; }

	private class GameLoop implements Runnable
	{
		long start_time;
        private long currentTime;
        private boolean isWon;

        @Override
		public void run ()
		{
			Initialize();
			while (isPlaying)
			{
                update();
                draw();
                if(isWon){
                    isPlaying                       = false;
                }
            }
        }

		private void Initialize() {
			this.start_time 						= System.nanoTime() /1000;
            this.isWon                              = false;
		}

		private void getDeltaTime() {
            currentTime 							= System.nanoTime() /1000;
            deltaTime 								= (float) (currentTime - start_time) / 1000;
            fpsCounter 								+= deltaTime;
            start_time 								= currentTime;
        }

        private void draw() {
            if((fpsCounter >= (1f/fpsCap) * 1000)){
				fps 								= Math.round(1000f / fpsCounter);
				gameController.updateFromModelToView();
				fpsCounter 							= 0;
			}
		}

		private void update() {
			getDeltaTime();

            if(_currentLevel != null){
                executeAction(_actionContainer.callSpecificAction(SpecificAction.POP_AND_CLEAR, null));

                if(_currentLevel.isActive) {
                    _currentLevel.update();
                    _moveContainer.move(shootingArea, deltaTime);
                } else {
                    if(checkLevelComplete()){
                        _currentLevel.nextLevel();

                        if(_currentLevel == null)   { this.isWon = true; }
                    } else {
                        _currentLevel.reloadLevel();
                    }
                }
            }
        }

		private boolean checkLevelComplete()		{ return _currentLevel.isComplete; }
	}
	
	public void executeAction (Action lastAction) {
		if (lastAction != null && !gun.outOfShots()) {
			gun.shoot();

            Unit currentUnit = _currentLevel.getCurrentUnit();
            if (currentUnit.getLocation().intersects(new Rectangle(lastAction.getX(), lastAction.getY(), 1, 1)))
            {
                _moveContainer.getShot();
                _currentLevel.hits++;
                _currentLevel.incrementTries();
                setScore(getScore() + currentUnit.getPoints().getValue());
            }
            else if (gun.outOfShots()) {
                if(!_currentLevel.getCurrentUnit().isShot){
                    _currentLevel.getCurrentUnit().setState(new FleeState());
                    _moveContainer.escaped();
                }
                _currentLevel.incrementTries();
            }
        }
	}

	public LevelFactory getLevelFactory()           { return gameController.levelFactory; }

	public UnitFactory getUnitFactory()             { return gameController.unitFactory; }

	public int getAmountOfShots ()			        { return gun.getAmountOfShots(); }
}
