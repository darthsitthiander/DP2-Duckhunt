package nl.avans.model.levels;

import nl.avans.model.Game;
import nl.avans.model.repositories.BehaviourFactory;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;

public abstract class BaseLevelState {
    public Game _game;
    public int ID;

    public int amountOfUnits;
    public int ducksToHit;
    public int currentUnitIndex;
    public int hits;
    public int tries;
    public Unit[] units;
    public boolean isComplete                               = false;
    public boolean isActive                                 = true;
    public String color;

    public BaseLevelState(Game game)                        {
        this._game  = game;
        this.color  = "#3FBFFF";
    }

    public abstract BaseLevelState clone();

    public void nextLevel()									{ _game.setLevel(_game.getLevelFactory().NextLevel(this)); }

    public void reloadLevel()								{ _game.setLevel(_game.getLevelFactory().getLevel(ID));	}

    public Unit getCurrentUnit()
    {
        Unit currentUnit = null;
        
        if(units.length > currentUnitIndex)					{ currentUnit = units[currentUnitIndex]; }
        
        return currentUnit;
    }

    public void update(){
        Unit currentUnit                                    =  units[currentUnitIndex];

        if (!currentUnit.isSpawn() || currentUnit.isGone())
        {
            if (currentUnit.isGone()) {
                currentUnitIndex++;
            }

            _game.gun.reload();

            if(currentUnitIndex < amountOfUnits)
            {
                currentUnit.setSpawn(_game.spawnArea);
                currentUnit.addBehaviour(_game._moveContainer, new BehaviourFactory());
            }
        }
        else {
            if(currentUnit.isGone){
                currentUnit.removeBehaviour();
            }
        }
    }

    public void incrementTries() {
        this.tries++;
    }

    public String toString(){
        return "BaseLevelState [ducksToHit: " + this.ducksToHit + ", hits: " + this.hits + "]";
    }
}
