package nl.avans.model.levels;

import nl.avans.model.Game;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;

public class GameLevel3 extends BaseLevelState
{
    public GameLevel3(Game game)
    {
        super(game);
        this.color = "#F8FB00";

        initLevel();
    }

    public void initLevel()
    {
        amountOfUnits										    = 4;
        this.ID                                                 = 3;
        this.currentUnitIndex 									= 0;
        this.units 												= new Unit[amountOfUnits];
        this.ducksToHit											= 3;

        this.units[0]											= UnitFactory.getInstance().getUnit("DuckBlack");
        this.units[1]											= UnitFactory.getInstance().getUnit("DuckBlue");
        this.units[2]											= UnitFactory.getInstance().getUnit("DuckRed");
        this.units[3]											= UnitFactory.getInstance().getUnit("DuckRed");
    }

    public BaseLevelState clone()								{ return new GameLevel3(_game); }

    public void update()
    {
        if(hits >= ducksToHit) this.isComplete = true;

        if(units[units.length - 1].isGone())					{ this.isActive = false; }

        if(isActive) super.update();
    }
}
