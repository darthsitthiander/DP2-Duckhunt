package nl.avans.model.levels;

import nl.avans.model.Game;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;

public class GameLevel1 extends BaseLevelState {

    public GameLevel1(Game game) {
        super(game);

        initLevel();
    }

    public void initLevel()
    {
        amountOfUnits										    = 1;
        this.ID                                                 = 1;
        this.currentUnitIndex 									= 0;
        this.units 												= new Unit[amountOfUnits];
        this.ducksToHit											= 1;

        this.units[0]											= UnitFactory.getInstance().getUnit("DuckBlack");
    }

    public BaseLevelState clone() {
        return new GameLevel1(_game);
    }

    public void update(){
        if(hits >= ducksToHit) this.isComplete = true;

        if(units[units.length - 1].isGone()){
            this.isActive = false;
        }

        if(isActive) super.update();
    }
}
