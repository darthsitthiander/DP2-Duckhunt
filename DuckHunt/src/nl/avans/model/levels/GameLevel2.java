package nl.avans.model.levels;

import nl.avans.model.Game;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;

public class GameLevel2 extends BaseLevelState {

    public GameLevel2(Game game) {
        super(game);
        this.color  = "#00F30B";

        initLevel();
    }

    public void initLevel() {
        amountOfUnits										    = 3;
        this.ID                                                 = 2;
        this.currentUnitIndex 									= 0;
        this.units 												= new Unit[amountOfUnits];
        this.ducksToHit											= 2;

        this.units[0]											= UnitFactory.getInstance().getUnit("DuckBlack");
        this.units[1]											= UnitFactory.getInstance().getUnit("DuckBlue");
        this.units[2]											= UnitFactory.getInstance().getUnit("DuckBlue");
    }

    public BaseLevelState clone() {
        return new GameLevel2(_game);
    }

    public void update(){
        if(hits >= ducksToHit) this.isComplete = true;

        if(units[units.length - 1].isGone()){
            this.isActive = false;
        }

        if(isActive) super.update();
    }
}
