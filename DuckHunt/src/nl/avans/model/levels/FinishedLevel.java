package nl.avans.model.levels;

        import nl.avans.model.Game;
import nl.avans.model.repositories.UnitFactory;
import nl.avans.model.screenparts.units.Unit;

public class FinishedLevel extends BaseLevelState {

    public FinishedLevel(Game game) {
        super(game);

        initLevel();
    }

    public void initLevel() {
        amountOfUnits										    = 1;
        this.ID                                                 = 0;
        this.currentUnitIndex 									= 0;
        this.units 												= new Unit[amountOfUnits];
        this.ducksToHit											= 0;

        this.units[0]											= UnitFactory.getInstance().getUnit("Dog");
    }

    public BaseLevelState clone() {
        return new FinishedLevel(_game);
    }

    public void update(){
        this.isComplete = true;
        
        if(this.isComplete && units[0].isDone())				{ this.isActive = false; }

        if(isActive)											{ super.update(); }
    }
}
