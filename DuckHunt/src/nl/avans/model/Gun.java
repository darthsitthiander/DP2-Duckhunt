package nl.avans.model;

public class Gun
{
	private int startAmountOfShots, amountOfShots;
	
	public Gun ()						{ this.startAmountOfShots = this.amountOfShots = 3; }
	
	public void shoot () {
		if (amountOfShots > 0) 			{--amountOfShots; }
	}

	public boolean outOfShots()			{ return (amountOfShots <= 0); }
	
	public void reload ()				{ this.amountOfShots = this.startAmountOfShots; }
	
	public int getAmountOfShots ()		{ return amountOfShots; }
}