package nl.avans.model.screenparts;

import nl.avans.model.levels.BaseLevelState;
import nl.avans.view.visitors.IUnitVisitor;
import java.awt.*;
import java.util.ArrayList;

public class Screen implements ScreenPart {
    ArrayList<ScreenPart> layers;

    public Graphics g;
    public int width;
    public int height;
    public BaseLevelState level;

    public Screen(BaseLevelState level, Graphics g, int width, int height){
        layers = new ArrayList<>();
        this.g = g;
        this.width = width;
        this.height = height;
        this.level = level;
    }

    public void accept(IUnitVisitor screenVisitor) {
        screenVisitor.visit(this);
        for(ScreenPart layer : this.layers){
            layer.accept(screenVisitor);
        }
    }

    public void addLayer(ScreenPart layer){
        this.layers.add(layer);
    }
}
