package nl.avans.model.screenparts;

import nl.avans.model.screenparts.units.Unit;
import nl.avans.view.visitors.IUnitVisitor;

import java.awt.*;

public class PostFX implements ScreenPart {
    public final Unit unit;
    public int height;
    public int width;
    public Graphics g;
    public final Image fly_away;

    public PostFX(Graphics g, int width, int height, Image fly_away, Unit unit){
        this.g              = g;
        this.width          = width;
        this.height         = height;
        this.fly_away       = fly_away;
        this.unit           = unit;
    }

    @Override
    public void accept(IUnitVisitor screenVisitor) {
        screenVisitor.visit(this);
    }
}
