package nl.avans.model.screenparts;

import nl.avans.controller.GameController;
import nl.avans.view.visitors.IUnitVisitor;

import java.awt.*;

public class HUD implements ScreenPart {

    public Image shootingAreaBackground;
    public Graphics g;
    public Image[] shots;
    public Image[] hits;
    public GameController controller;
    public Image score;

    public HUD(Image shootingAreaBackground, Graphics g, Image[] shots, Image[] hits, Image score, Image fly_away, GameController controller){
        this.shootingAreaBackground = shootingAreaBackground;
        this.g = g;
        this.shots = shots;
        this.controller = controller;
        this.hits = hits;
        this.score = score;
    }

    @Override
    public void accept(IUnitVisitor screenVisitor) {
        screenVisitor.visit(this);
    }
}