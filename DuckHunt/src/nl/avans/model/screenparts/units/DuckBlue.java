package nl.avans.model.screenparts.units;

import nl.avans.enums.DuckPoints;
import nl.avans.enums.DuckWing;

import javax.swing.*;
import java.awt.*;

public class DuckBlue extends Duck {

    public DuckBlue ()
    {
        super(0.15f, -0.06f, DuckPoints.MID);

        this.animationSpeed                     = 0.3f;
        wings									= new Image[8];
        wings[DuckWing.TOP.getValue()]			= new ImageIcon("resources/units/duck/blue/Duck_Straight_1.png").getImage();
        wings[DuckWing.MID.getValue()]			= new ImageIcon("resources/units/duck/blue/Duck_Straight_2.png").getImage();
        wings[DuckWing.BOTTOM.getValue()]		= new ImageIcon("resources/units/duck/blue/Duck_Straight_3.png").getImage();
        wings[DuckWing.HIT.getValue()]			= new ImageIcon("resources/units/duck/blue/Duck_Hit.png").getImage();
        wings[DuckWing.FALLING.getValue()]		= new ImageIcon("resources/units/duck/blue/Duck_Falling.png").getImage();
        wings[DuckWing.AWAY_TOP.getValue()] 	= new ImageIcon("resources/units/duck/blue/Duck_Away_1.png").getImage();
        wings[DuckWing.AWAY_MID.getValue()]	    = new ImageIcon("resources/units/duck/blue/Duck_Away_2.png").getImage();
        wings[DuckWing.AWAY_BOTTOM.getValue()]	= new ImageIcon("resources/units/duck/blue/Duck_Away_3.png").getImage();
    }

    public String toString(){
        return "DuckBlue";
    }
}
