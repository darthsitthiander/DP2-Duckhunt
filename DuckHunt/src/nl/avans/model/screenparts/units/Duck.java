package nl.avans.model.screenparts.units;

import java.awt.*;
import java.util.Random;
import nl.avans.enums.DuckPoints;
import nl.avans.enums.DuckWing;
import nl.avans.model.repositories.BehaviourFactory;
import nl.avans.model.repositories.containers.MoveContainer;
import nl.avans.model.repositories.states.FlyingState;
import nl.avans.view.visitors.IUnitVisitor;

public class Duck extends Unit {

    public Duck() {
        super();
    }

    public Duck(float xSpeed, float ySpeed, DuckPoints duckPoints) {
        this();
        this.xSpeed							= xSpeed;
        this.ySpeed							= ySpeed;
        this.duckWing						= DuckWing.TOP;
        this.points							= duckPoints;
        this.width							= 34;
        this.height							= 31;
        this.setState(new FlyingState());
    }

    public void addBehaviour(MoveContainer mc, BehaviourFactory bf){
        _moveBehaviour = bf.createMoveBehaviour(this);
        mc.add(_moveBehaviour);
    }

    public void accept(IUnitVisitor visitor)	{ visitor.visit(this); }

	public DuckWing getUnitWing()			{ return duckWing; }
	
	public Rectangle getLocation ()			{ return new Rectangle((x + (width/2)), (y + (height/2)), width, height); }

	public void setSpawn(Rectangle spawnArea)
	{
		Random random 						= new Random();
		
		x									= random.nextInt(((spawnArea.width - (spawnArea.x + (width * 2))) - (width * 2)) + 1) + (spawnArea.x + (width * 2));
		y									= spawnArea.y - (height/2);
		isSpawn 							= true;
	}

    public void changeUnitWing()
    {
        switch (this.duckWing)
        {
            case TOP:
            case BOTTOM:
                this.duckWing 			= DuckWing.MID;
                break;
            case MID:
                if (this.wingsGoUp)		{ this.duckWing = DuckWing.TOP; }
                else					{ this.duckWing = DuckWing.BOTTOM; }

                this.wingsGoUp = !this.wingsGoUp;
                break;
            case HIT:
                this.duckWing			= DuckWing.FALLING;
                break;
            case AWAY_TOP:
            case AWAY_BOTTOM:
                this.duckWing			= DuckWing.AWAY_MID;
                break;
            case AWAY_MID:
                if (this.wingsGoUp)		{ this.duckWing = DuckWing.AWAY_TOP; }
                else					{ this.duckWing = DuckWing.AWAY_BOTTOM; }

                this.wingsGoUp = !this.wingsGoUp;
                break;
            default:
                break;

        }
    }

    public String toString(){
        return "Duck[]";
    }
}