package nl.avans.model.screenparts.units;

import nl.avans.enums.DuckPoints;
import nl.avans.enums.DuckWing;

import javax.swing.*;
import java.awt.*;

public class DuckBlack extends Duck {

    public DuckBlack ()
    {
        super(0.05f, -0.03f, DuckPoints.HIGH);

        this.animationSpeed                     = 0.5f;
        wings									= new Image[8];
        wings[DuckWing.TOP.getValue()]			= new ImageIcon("resources/units/duck/black/Duck_Straight_1.png").getImage();
        wings[DuckWing.MID.getValue()]			= new ImageIcon("resources/units/duck/black/Duck_Straight_2.png").getImage();
        wings[DuckWing.BOTTOM.getValue()]		= new ImageIcon("resources/units/duck/black/Duck_Straight_3.png").getImage();
        wings[DuckWing.HIT.getValue()]			= new ImageIcon("resources/units/duck/black/Duck_Hit.png").getImage();
        wings[DuckWing.FALLING.getValue()]		= new ImageIcon("resources/units/duck/black/Duck_Falling.png").getImage();
        wings[DuckWing.AWAY_TOP.getValue()] 	= new ImageIcon("resources/units/duck/black/Duck_Away_1.png").getImage();
        wings[DuckWing.AWAY_MID.getValue()]	    = new ImageIcon("resources/units/duck/black/Duck_Away_2.png").getImage();
        wings[DuckWing.AWAY_BOTTOM.getValue()]	= new ImageIcon("resources/units/duck/black/Duck_Away_3.png").getImage();
    }

    public String toString(){
        return "DuckBlack";
    }
}
