package nl.avans.model.screenparts.units;

import nl.avans.enums.DuckWing;
import nl.avans.enums.Points;
import nl.avans.model.Gun;
import nl.avans.model.screenparts.ScreenPart;
import nl.avans.model.repositories.BehaviourFactory;
import nl.avans.model.repositories.behaviour.MoveBehaviour;
import nl.avans.model.repositories.containers.MoveContainer;
import nl.avans.model.repositories.states.State;

import java.awt.*;
import java.util.Iterator;

public abstract class Unit implements Cloneable, ScreenPart {

    public int x, y, currentInterval, minInterval, maxInterval, width, height;
    public float xSpeed, ySpeed, xPending, yPending;
    public Points points;
    public DuckWing duckWing;
    public boolean isSpawn, isShot, isGone, isEscaped, leavedSpawnZone, wingsGoUp;
    protected Image[] wings = new Image[8];
    public MoveContainer _moveContainer;
    protected MoveBehaviour _moveBehaviour;
    public Graphics g;
    public int xDeath;
    public int yDeath;
    private boolean isDone;
    private State state;
    public float animationSpeed;

    public Unit(){
        this.isSpawn 						= false;
        this.isShot							= false;
        this.leavedSpawnZone				= false;
        this.wingsGoUp 						= false;
        this.isGone							= false;
        this.isEscaped						= false;
        this.isDone                         = false;
        this.currentInterval 				= 0;
        this.xDeath                         = -1;
        this.yDeath                         = -1;
        this.animationSpeed                 = 0;
        this.minInterval					= 3;
        this.maxInterval					= 5;
    }


    public abstract void addBehaviour(MoveContainer mc, BehaviourFactory bf);

    public Unit clone() {

        Unit clone = null;

        try {
            clone = (Unit) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return clone;
    }

    public int getX ()						{ return x; }
    public int getY ()						{ return y; }

    public double getXSpeed ()				{ return xSpeed; }
    public double getYSpeed ()				{ return ySpeed; }

    public boolean isSpawn ()				{ return isSpawn; }
    public boolean isGone ()				{ return isGone; }
    public boolean isEscaped ()				{ return isEscaped; }

    public DuckWing getUnitWing() {
        return null;
    }

    public abstract void setSpawn(Rectangle spawnArea);
    public abstract Rectangle getLocation();

    public void removeBehaviour() {
    	MoveBehaviour value;
        for (Iterator<MoveBehaviour> iterator = this._moveContainer.getBehaviours().iterator(); iterator.hasNext(); ) {
            value = iterator.next();
            if (value == this._moveBehaviour) {
                iterator.remove();
            }
        }
    }

    public Image getUnitWing(int number){
        return this.wings[number];
    }

    public void setGraphics(Graphics g) {
        this.g = g;
    }

    public Points getPoints(){
        return this.points;
    }

    public String toString(){
        return "Unit [x:"+this.x+", y:"+this.x+", currentInterval:"+this.x+", minInterval:"+this.x+", maxInterval:"+this.x+", deathInterval:"+this.x+", width:"+this.x+", height:"+this.x+",\nisSpawn:"+this.x+", isShot:"+this.x+", isGone:"+this.x+", isEscaped:"+this.x+"]";
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        this.isDone = done;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void changeUnitWing(){

    }
}
