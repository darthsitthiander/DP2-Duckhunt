package nl.avans.model.screenparts.units;

import nl.avans.enums.DuckPoints;
import nl.avans.model.repositories.BehaviourFactory;
import nl.avans.model.repositories.containers.MoveContainer;
import nl.avans.model.repositories.states.AppearState;
import nl.avans.view.visitors.IUnitVisitor;

import javax.swing.*;
import java.awt.*;

public class Dog extends Unit {
    public Image image;

    public Dog() {
        super();
        this.xSpeed							= 0.05f;
        this.ySpeed							= -0.03f;
        this.width							= 80;
        this.height							= 50;
        this.x								= 200;
        this.y								= 200;
        this.animationSpeed                 = 0.01f;
        this.setState(new AppearState());

        this.points = DuckPoints.MID;
        this.image = new ImageIcon("resources/dog.png").getImage();
    }

    @Override
    public void addBehaviour(MoveContainer mc, BehaviourFactory bf) {
        _moveBehaviour = bf.createMoveBehaviour(this);
        mc.add(_moveBehaviour);
    }

    @Override
    public void setSpawn(Rectangle spawnArea) {
        x									= spawnArea.x;
        y									= spawnArea.y - (height/2);
        isSpawn 							= true;
    }

    @Override
    public Rectangle getLocation ()			{ return new Rectangle(x, y, width, height); }

    @Override
    public void accept(IUnitVisitor visitor){
        visitor.visit(this);
    }

    public String toString(){
        return "Dog";
    }
}
