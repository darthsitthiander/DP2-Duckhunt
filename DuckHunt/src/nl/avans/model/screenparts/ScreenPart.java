package nl.avans.model.screenparts;

import nl.avans.view.visitors.IUnitVisitor;

public interface ScreenPart {
    public void accept(IUnitVisitor screenVisitor);

    public String toString();
}
