package nl.avans.model;

import java.util.List;
import java.util.Vector;

import nl.avans.enums.SpecificAction;

public class ActionContainer
{
	private List<Action> actions;
	
	public ActionContainer ()					{ this.actions = new Vector<>(); }
	
	public synchronized Action callSpecificAction(SpecificAction specificAction, Action action)
	{
		Action returnAction = null;

		switch (specificAction)
		{
			case ADD:
				actions.add(action);
				break;
			case POP_AND_CLEAR:
				if (this.actions.size() > 0)
				{
					returnAction = actions.get(actions.size() - 1);
					actions.clear();
				}
				break;
		}

		return returnAction;
	}
}
