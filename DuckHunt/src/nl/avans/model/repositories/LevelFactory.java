package nl.avans.model.repositories;

import nl.avans.model.Game;
import nl.avans.model.levels.BaseLevelState;
import nl.avans.model.LevelMap;
import nl.avans.model.levels.FinishedLevel;

import java.util.Map;

public class LevelFactory {

    private static LevelMap levelMap;
    private static Game _game;

    public LevelFactory(Game game)
    {
        this._game      = game;
        this.levelMap   = new LevelMap(this._game);
    }

    public static BaseLevelState NextLevel(BaseLevelState currentLevel)
    {
        if (currentLevel == null || currentLevel.ID > levelMap.size() || currentLevel.ID == 0) 
        {
        	return null;
        }

        int index										= currentLevel.ID;

        BaseLevelState level							= null;
        
        if(index < levelMap.size())						{ level = getLevel(index + 1); }
        
        if(level == null)								{ level = Finished(); }
        
        return level;
    }

    public static BaseLevelState Finished()				{ return new FinishedLevel(_game); }

    public static Map GetMap()							{ return levelMap; }

    public static BaseLevelState getLevel(int levelID)	{ return levelMap.get(levelID).clone(); }

    public BaseLevelState getFirstLevel() 				{ return levelMap.get(1); }
}
