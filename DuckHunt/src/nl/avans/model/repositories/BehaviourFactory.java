package nl.avans.model.repositories;

import nl.avans.model.repositories.behaviour.GameOverBehaviour;
import nl.avans.model.repositories.behaviour.TargetBehaviour;
import nl.avans.model.screenparts.units.Dog;
import nl.avans.model.screenparts.units.Duck;

public class BehaviourFactory {
    
    public TargetBehaviour createMoveBehaviour (Duck duck) {
        return new TargetBehaviour(duck);
    }

    public GameOverBehaviour createMoveBehaviour(Dog dog){
        return new GameOverBehaviour(dog);
    }

}
