package nl.avans.model.repositories.states;

import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;

public class AppearState implements State {

    private float animationInterval     = 0f;
    private float animationRest         = 0f;
    private float limit;


    @Override
    public void move(Unit unit, Rectangle shootArea, float deltaTime) {
        if(unit.y > 105){
            animationInterval += deltaTime;
            limit = (unit.animationSpeed * 500) - animationRest;
            if(animationInterval >= limit){
                unit.y -= 1;
                unit.x = 95;

                animationRest = animationInterval % limit;
                animationInterval = 0;
            }
        } else if(unit.y == 105) {
            unit.setDone(true);
        }
    }

    @Override
    public void drawIndicator(HUD hud, int i) {

    }
}
