package nl.avans.model.repositories.states;

import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;

public interface State {
    void move(Unit unit, Rectangle shootArea, float deltaTime);
    void drawIndicator(HUD hud, int i);
}
