package nl.avans.model.repositories.states;

import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;
import java.util.Random;

public class FlyingState implements State {

    private float animationInterval     = 0f;
    private float animationRest         = 0f;

    private float limit;

    @Override
    public void move(Unit unit, Rectangle shootArea, float deltaTime) {
        unit.xPending 					+= unit.xSpeed * deltaTime;
        unit.yPending 					+= unit.ySpeed * deltaTime;

        while (unit.xPending < -1 || unit.xPending > 1)
        {
            unit.x 							+= (int) unit.xPending;
            unit.xPending 					%= 1;
        }

        while (unit.yPending < -1 || unit.yPending > 1)
        {
            unit.y 							+= (int)unit.yPending;
            unit.yPending 					%= 1;

        }

        Random random 					= new Random();

        if (++unit.currentInterval >= unit.minInterval && unit.currentInterval == random.nextInt(unit.maxInterval))
        {
            if (random.nextBoolean())	{ unit.xSpeed *= -1; }

            if (random.nextBoolean())	{ unit.ySpeed *= -1; }

            unit.currentInterval 		= 0;
        }

        if ((unit.x + (unit.width * 0.5)) <= (0 - (int) unit.xSpeed) && unit.xSpeed < 0
                || (unit.x + (unit.width * 1.5)) >= (shootArea.width - (int) unit.xSpeed) && unit.xSpeed > 0)						{ unit.xSpeed *= -1; }

        if ((unit.y + (unit.height * 0.5)) <= (0 - (int) unit.ySpeed) && unit.ySpeed < 0
                || unit.leavedSpawnZone && (unit.y + (unit.height * 1.5)) >= (shootArea.height - (int) unit.ySpeed) && unit.ySpeed > 0)	{ unit.ySpeed *= -1; }


        if (!unit.leavedSpawnZone && unit.y + (unit.height * 1.5) < (shootArea.height - unit.ySpeed)) 									{ unit.leavedSpawnZone = true; }
        if (!unit.leavedSpawnZone && unit.y > (shootArea.height + unit.height) && unit.ySpeed > 0) 										{ unit.ySpeed *= -1; }

        animationInterval += deltaTime;
        limit = (unit.animationSpeed * 500) - animationRest;
        if(animationInterval >= limit){
            unit.changeUnitWing();

            animationRest = animationInterval % limit;
            animationInterval = 0;
        }
    }

    @Override
    public void drawIndicator(HUD hud, int i) {
        hud.g.drawImage(hud.hits[2], 95 + i*8, 209, 7, 7, null);
    }
}
