package nl.avans.model.repositories.states;

import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;
import java.util.Random;

public class FleeState implements State {

    private float animationInterval     = 0f;
    private float animationRest         = 0f;

    private float limit;

    @Override
    public void move(Unit unit, Rectangle shootArea, float deltaTime) {
        unit.xPending 					+= unit.xSpeed * deltaTime;
        unit.yPending 					+= unit.ySpeed * deltaTime;

        while (unit.xPending < -1 || unit.xPending > 1)
        {
            unit.x 							+= (int) unit.xPending;
            unit.xPending 					%= 1;
        }

        while (unit.yPending < -1 || unit.yPending > 1)
        {
            unit.y 							+= (int)unit.yPending;
            unit.yPending 					%= 1;

        }

        if (unit.isEscaped)
        {
            if (unit.y < -unit.height)		{ unit.isGone = true; }
        }

        if (!unit.leavedSpawnZone && unit.y + (unit.height * 1.5) < (shootArea.height - unit.ySpeed)) 									{ unit.leavedSpawnZone = true; }
        if (!unit.leavedSpawnZone && unit.y > (shootArea.height + unit.height) && unit.ySpeed > 0) 										{ unit.ySpeed *= -1; }

        animationInterval += deltaTime;
        limit = (unit.animationSpeed * 500) - animationRest;
        if(animationInterval >= limit){
            unit.changeUnitWing();

            animationRest = animationInterval % limit;
            animationInterval = 0;
        }
    }

    @Override
    public void drawIndicator(HUD hud, int i) {
        hud.g.drawImage(hud.hits[2], 95 + i*8, 209, 7, 7, null);
    }
}
