package nl.avans.model.repositories.states;

import nl.avans.enums.DuckWing;
import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.units.Unit;
import java.awt.*;

public class ShotState implements State {

    private long timeOfMove                     = -1;
    private float deathInterval                 = 0;

    @Override
    public void move(Unit unit, Rectangle shootArea, float deltaTime) {
        deathInterval                      += deltaTime;
        if(deathInterval >= 500){

            if (unit.duckWing == DuckWing.HIT)	{ unit.changeUnitWing(); }

            if(timeOfMove == -1) timeOfMove = System.nanoTime();
            if(System.nanoTime() > (timeOfMove + 25000000)){
                unit.y			                += 5;
                timeOfMove                      = System.nanoTime();
            }

            if (unit.y > shootArea.height) 	{
                unit.isGone                     = true;
            }
        }
    }

    @Override
    public void drawIndicator(HUD hud, int i)   { hud.g.drawImage(hud.hits[1], 95 + i*8, 209, 7, 7, null); }

}
