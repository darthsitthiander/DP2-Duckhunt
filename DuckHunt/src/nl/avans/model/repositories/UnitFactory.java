package nl.avans.model.repositories;

import nl.avans.model.screenparts.units.*;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class UnitFactory
{
	private static UnitFactory instance;
	private ServiceLoader<Unit> loader;
	
	private UnitFactory ()								{ loader = ServiceLoader.load(Unit.class); }
	
	public static synchronized UnitFactory getInstance()
	{
		if (instance == null)							{ instance = new UnitFactory(); }

		return instance;
	}
	
	public Unit getUnit (String unitName)
	{
		Unit unitStrategy								= null;
		Unit unit;
		
		try
		{
			Iterator<Unit> unitStrategies				= this.loader.iterator();

			while (unitStrategy == null && unitStrategies.hasNext())
			{
				unit									= unitStrategies.next();

				if (unit.getClass().getSimpleName().equalsIgnoreCase(unitName))
				{
					unitStrategy						= unit.clone();
				}
			}
		}
		catch (ServiceConfigurationError serviceError)
		{
			System.out.println("ServiceConfigurationError: " + serviceError.getMessage() + ". Exiting program.");
			System.exit(1);
		}
		
		
		return unitStrategy;
	}
}
