package nl.avans.model.repositories.containers;

import nl.avans.model.repositories.behaviour.MoveBehaviour;

import java.awt.*;
import java.util.ArrayList;

public class MoveContainer {
    private ArrayList<MoveBehaviour> behaviours         = new ArrayList<>();

    public void add(MoveBehaviour moveBehaviour)        { this.behaviours.add(moveBehaviour); }

    public void remove(MoveBehaviour moveBehaviour)     { this.behaviours.remove(moveBehaviour); }

    public ArrayList<MoveBehaviour> getBehaviours()     { return this.behaviours; }

    public void move(Rectangle shootArea, float deltaTime) {
        for(MoveBehaviour mb : this.getBehaviours()){
            mb.move(shootArea, deltaTime);
        }
    }

    public void getShot() {
        for(MoveBehaviour mb : this.getBehaviours())    { mb.getShot(); }
    }

    public void escaped() {
        for(MoveBehaviour mb : this.getBehaviours())    { mb.escaped(); }
    }
}
