package nl.avans.model.repositories.behaviour;

import nl.avans.enums.DuckWing;
import nl.avans.enums.Points;
import nl.avans.model.repositories.states.ShotState;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;
import java.util.Random;

public class TargetBehaviour implements MoveBehaviour {

    private final Unit unit;

    public TargetBehaviour(Unit unit){
        this.unit = unit;
    }

    public void move (Rectangle shootArea, float deltaTime)
    {
        if (!unit.isSpawn || unit.isGone)		{ return; }

        unit.getState().move(unit, shootArea, deltaTime);
    }

    @Override
    public void escaped() {

        unit.isEscaped							= true;

        this.unit.duckWing						= DuckWing.AWAY_TOP;
        this.unit.wingsGoUp 					= false;
        this.unit.ySpeed						= -0.15f;
        this.unit.xPending						= 0;
        this.unit.yPending						= 0;

        if (new Random().nextBoolean())		    { this.unit.xSpeed = 0.15f; }
        else								    { this.unit.xSpeed = -0.15f; }
    }

    public Points getShot(){
        unit.setState(new ShotState());
        unit.isShot                             = true;

        this.unit.duckWing						= DuckWing.HIT;

        return this.unit.points;
    }
}
