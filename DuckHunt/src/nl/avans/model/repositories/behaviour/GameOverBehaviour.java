package nl.avans.model.repositories.behaviour;

import nl.avans.enums.Points;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;

public class GameOverBehaviour implements MoveBehaviour {

    private final Unit unit;

    public GameOverBehaviour(Unit unit){
        this.unit = unit;
    }

    @Override
    public void move(Rectangle shootArea, float deltaTime) {
        if (!unit.isSpawn || unit.isGone)				{ return; }

        unit.getState().move(unit, shootArea, deltaTime);
    }

    @Override
    public void escaped() {
    }

    @Override
    public Points getShot() {
        return null;
    }
}
