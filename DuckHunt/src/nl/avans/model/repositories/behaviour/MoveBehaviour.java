package nl.avans.model.repositories.behaviour;

import nl.avans.enums.Points;

import java.awt.*;

public interface MoveBehaviour {
    public void move(Rectangle shootArea, float deltaTime);
    public void escaped();
    public Points getShot();
}
