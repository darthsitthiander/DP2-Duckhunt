package nl.avans.model;

import nl.avans.model.levels.*;

import java.util.*;

public class LevelMap extends HashMap<Integer, BaseLevelState> {

    public LevelMap(Game game)
    {
        this.put(new GameLevel1(game));
        this.put(new GameLevel2(game));
        this.put(new GameLevel3(game));
    }

    public void put(BaseLevelState level){
        this.put(level.ID, level);
    }
}
