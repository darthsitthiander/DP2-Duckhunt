package nl.avans.model;

public class Action
{
	private int x, y;
	
	public Action (int x, int y)
	{
		this.x 					= x;
		this.y 					= y;
	}
	
	public int getX () 			{return this.x; }
	public int getY () 			{return this.y; }
}
