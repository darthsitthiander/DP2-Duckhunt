package nl.avans.view.visitors;

import nl.avans.model.levels.BaseLevelState;
import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.PostFX;
import nl.avans.model.screenparts.Screen;
import nl.avans.model.screenparts.units.Dog;
import nl.avans.model.screenparts.units.Duck;
import nl.avans.model.screenparts.units.Unit;

import java.awt.*;

public class DrawVisitor implements IUnitVisitor {

    @Override
    public void visit(HUD hud) {
        boolean lastLevel = false;
        if(hud.controller.game._currentLevel != null) { lastLevel = (hud.controller.game._currentLevel.ID == 0); }

        hud.g.drawImage(hud.shootingAreaBackground, 0, 0, null);
        
        if(!lastLevel) { hud.g.drawImage(hud.shots[hud.controller.getAmountOfShots()], 23, 207, null); }

        BaseLevelState currentLvl = hud.controller.game._currentLevel;


        if(currentLvl != null){
            if(!lastLevel){
                hud.g.setColor(Color.decode("#86D809"));
                hud.g.setFont(new Font("Arial", Font.PLAIN, 9));
                hud.g.drawString("R = " + currentLvl.ID, 24, 200);
            }

            hud.g.setFont(new Font("Comic Sans", Font.BOLD, 11));
            hud.g.setColor(Color.decode("#FFFFFF"));
            hud.g.drawImage(hud.score, 196, 216, 39, 7, null);
            hud.g.drawString(String.valueOf(hud.controller.game.getScore()), 203, 215);

            if(!lastLevel){
                hud.g.drawImage(hud.hits[3], 63, 208, 23, 7, null);
                for(int i=0; i<currentLvl.amountOfUnits && i<10; i++){
                    if(i < currentLvl.tries){
                        currentLvl.units[i].getState().drawIndicator(hud, i);
                    } else {
                        hud.g.drawImage(hud.hits[0], 95 + i * 8, 209, 7, 7, null);
                    }
                }
            }

            if(!lastLevel && hud.controller.game.showFPS) { hud.g.drawString("FPS " + hud.controller.game.fps, 210, 15); }


            Unit unit = currentLvl.getCurrentUnit();

            if(unit != null && unit.isShot){
                if(unit.xDeath == -1){
                    unit.xDeath = unit.getX() + ( unit.width ) / 4*3;
                }
                if(unit.yDeath == -1){
                    unit.yDeath = unit.getY() + unit.height;
                }
                hud.g.drawString(String.valueOf(unit.getPoints().getValue()), unit.xDeath, unit.yDeath);
            }
        }
    }

    @Override
    public void visit(Screen screen) {
        Color color = Color.decode("#3FBFFF");
        if(screen.level != null){
            color = Color.decode(screen.level.color);
        }
        screen.g.setColor(color);
        screen.g.fillRect(0, 0, screen.width, screen.height);
    }

    @Override
     public void visit(Duck duck) {
        Rectangle duckLocation 	= duck.getLocation();

        if (duck.getXSpeed() > 0)				{ duck.g.drawImage(duck.getUnitWing(duck.getUnitWing().getValue()), duckLocation.x, duckLocation.y, duckLocation.width, duckLocation.height, null); }
        else									{ duck.g.drawImage(duck.getUnitWing(duck.getUnitWing().getValue()), (duckLocation.x + duckLocation.width), duckLocation.y, -duckLocation.width, duckLocation.height, null); }
    }

    @Override
    public void visit(Dog dog) {
        Rectangle dogLocation = dog.getLocation();
        dog.g.drawImage(dog.image, dogLocation.x, dogLocation.y, dogLocation.width, dogLocation.height, null);
    }

    @Override
    public void visit(PostFX postfx) {
        postfx.g.setColor(Color.decode("#FFBFB3"));
        postfx.g.fillRect(0, 0, postfx.width, postfx.height);
        postfx.g.drawImage(postfx.fly_away, (postfx.width / 2) - postfx.unit.width, (postfx.height / 2) - postfx.unit.height, null);
    }
}
