package nl.avans.view.visitors;

import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.PostFX;
import nl.avans.model.screenparts.Screen;
import nl.avans.model.screenparts.units.Dog;
import nl.avans.model.screenparts.units.Duck;

public interface IUnitVisitor {

    public void visit(HUD hud);
    public void visit(Screen screen);
    public void visit(Duck duck);
    public void visit(Dog dog);
    public void visit(PostFX postfx);

}
