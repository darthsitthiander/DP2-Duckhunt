package nl.avans.view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import nl.avans.controller.GameController;

@SuppressWarnings("serial")
public class MainFrame extends JFrame
{
	private GameController controller;
	private GamePanel gamePanel;
	
	public MainFrame (GameController newController)
	{
		controller 						= newController;

		setTitle("Duck Hunt");
		setIconImage(new ImageIcon("resources/dog.png").getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}
	
	public void buildAndShowGui ()
	{
		gamePanel 						= new GamePanel(controller);
		Dimension screenDimension 		= Toolkit.getDefaultToolkit().getScreenSize();

		controller.addObserver(gamePanel);
		setContentPane(gamePanel);
		pack();
		setLocation((screenDimension.width / 2) - (getSize().width / 2), (screenDimension.height / 2) - (getSize().height / 2));
		setVisible(true);
	}
}