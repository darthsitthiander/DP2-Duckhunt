package nl.avans.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import nl.avans.controller.GameController;

@SuppressWarnings("serial")
public class StartButton extends JLabel
{
	public StartButton (GameController controller)
	{
		setText("Start");
		setFont(new Font("Ariel", Font.BOLD, 18));
		setForeground(Color.white);
		setBounds(193, 204, 50, 20);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				controller.startGame();
				setVisible(false);
			}
		});
	}
}
