package nl.avans.view;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import nl.avans.controller.GameController;
import nl.avans.model.screenparts.HUD;
import nl.avans.model.screenparts.PostFX;
import nl.avans.model.screenparts.Screen;
import nl.avans.model.screenparts.units.Unit;
import nl.avans.view.visitors.DrawVisitor;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Observer
{
	private final Image score;
	private final Image fly_away;
	private GameController controller;
	private Image shootingAreaBackground;
	private Image[] shots;
	private Image[] hits;
	private StartButton startLabel;
	
	public GamePanel (GameController newController)
	{
		controller 										= newController;
		
		shootingAreaBackground							= new ImageIcon("resources/ShootingArea.png").getImage();
		
		startLabel 										= new StartButton(newController);
		startLabel.setVisible(true);
		add(startLabel);
		
		shots											= new Image[4];
		shots[0]										= new ImageIcon("resources/Shots_0.png").getImage();
		shots[1]										= new ImageIcon("resources/Shots_1.png").getImage();
		shots[2]										= new ImageIcon("resources/Shots_2.png").getImage();
		shots[3]										= new ImageIcon("resources/Shots_3.png").getImage();

		hits											= new Image[4];
		hits[0]											= new ImageIcon("resources/hit_white.png").getImage();
		hits[1]											= new ImageIcon("resources/hit_red.png").getImage();
		hits[2]											= new ImageIcon("resources/hit_grey.png").getImage();
		hits[3]											= new ImageIcon("resources/hit.png").getImage();

		score											= new ImageIcon("resources/score.png").getImage();

		fly_away										= new ImageIcon("resources/fly_away.png").getImage();

		addMouseListener(controller.getMouseAdapter());

		setPreferredSize(new Dimension(256,240));
		setLayout(null);
	}
	
	public void paintComponent (Graphics g)
	{
		super.paintComponent(g);
        Screen screen = new Screen(controller.game._currentLevel, g, getWidth(), getHeight());

		if (  controller.game._currentLevel != null && controller.game.isPlaying){
			Unit unit										= controller.getCurrentUnit();
            if (unit != null)
			{
                if (unit.isEscaped())
				{
                    screen.addLayer(new PostFX(g, getWidth(), getHeight(), this.fly_away, unit));
                }

                unit.setGraphics(g);
                screen.addLayer(unit);
			}
		}
        screen.addLayer(new HUD(shootingAreaBackground, g, shots, hits, score, fly_away, controller));

        screen.accept(new DrawVisitor());
    }
	
	@Override
	public void update (Observable arg0, Object arg1)
	{
		repaint();
	}
}
