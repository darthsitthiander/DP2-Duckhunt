package nl.avans.enums;

public enum DuckPoints implements Points
{
	HIGH(1500),
	MID(1000),
	LOW(500);
	
	private final int value;
	
	private DuckPoints (int value)	{ this.value = value; }
	
	public int getValue () 			{ return this.value; }
}
