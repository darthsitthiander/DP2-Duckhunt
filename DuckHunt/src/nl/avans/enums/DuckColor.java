package nl.avans.enums;

public enum DuckColor
{
	BLACK,
	RED,
	BLUE
}
