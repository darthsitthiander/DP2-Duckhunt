package nl.avans.enums;

public enum DuckWing
{
	TOP(0),
	MID(1),
	BOTTOM(2),
	HIT(3),
	FALLING(4),
	AWAY_TOP(5),
	AWAY_MID(6),
	AWAY_BOTTOM(7);
	
	private final int value;
	
	private DuckWing (int value)	{ this.value = value; }
	
	public int getValue () 			{ return this.value; }
}
